package a20014022.thequiz;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by Ale on 02/02/2018.
 */

public class FormActivity extends AppCompatActivity {

    Button submit;
    String auth;
    EditText eMail;
    EditText passwd;
    EditText rePasswd;
    final String TAG = "AUTT";
    private FirebaseDatabase database;// remote DB
    private DatabaseReference myRef; // reference to a location
    private FirebaseAuth mAuth; // authorization
    Button registrazione;
    private ProgressDialog mProgressDialog;
    Button clear;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // initialization in onCreate()
        mAuth = FirebaseAuth.getInstance();
        settaLayoutLogin();
    }


    @Override
    public void onStart(){
        super.onStart();
        Log.i(TAG,"onStart FormActivity");
        if(mAuth.getCurrentUser()!=null){
            database = FirebaseDatabase.getInstance();
            myRef = database.getReference("users/"+mAuth.getCurrentUser().getUid());
            // Read from the database
            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    // This method is called once with the initial value and again
                    // whenever data at this location is updated.
                    User user = dataSnapshot.getValue(User.class);
                    Log.i(TAG, "Value is: " + user.toString());
                    Toast.makeText(FormActivity.this.getApplicationContext(),"Bentornato, "+user.getNick()+"!",Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                    Log.i(TAG, "Failed to read value.", error.toException());
                }
            });
            startActivity(new Intent(FormActivity.this,MainActivity.class));
            finish();
        }
    }

    private void settaLayoutLogin(){
        setContentView(R.layout.authform_login);
        submit = (Button)findViewById(R.id.submit);
        eMail = (EditText) findViewById(R.id.edit_text);
        passwd = (EditText)findViewById(R.id.edit_text2);
        registrazione = (Button)findViewById(R.id.registra);
        auth = "login";
        submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(validateForm()){
                    signIn(eMail.getText().toString(),passwd.getText().toString());
                }
                else{
                    Toast.makeText(FormActivity.this.getApplicationContext(),"Parametri errati",Toast.LENGTH_SHORT).show();
                }
            }
        });
        registrazione.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(FormActivity.this,RegistrazioneActivity.class));
            }

        });
        clear = (Button)findViewById(R.id.clear);
        clear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                eMail.setText("");
                passwd.setText("");
            }

        });
    }

    protected boolean validateForm(){
        boolean result = true;
        if(TextUtils.isEmpty(eMail.getText().toString())){
            eMail.setError("Required");
            result = false;
        } else {
            eMail.setError(null);
        }
        if(TextUtils.isEmpty(passwd.getText().toString())){
            passwd.setError("Required");
            result = false;
        } else {
            passwd.setError(null);
        }
        if(auth.equals("registrazione")) {
            if (TextUtils.isEmpty(rePasswd.getText().toString())) {
                rePasswd.setError("Required");
                result = false;
            } else {
                rePasswd.setError(null);
            }
            if (!passwd.getText().toString().equals(rePasswd.getText().toString())) {
                Toast.makeText(this, "Le due passwrod non combaciano", Toast.LENGTH_SHORT).show();
                result = false;
            }
        }
        return result;
    }



    protected void showProgressDialog(){
        if(mProgressDialog == null){
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Attendi...");
        }
        mProgressDialog.show();
    }

    protected void hideProgressDialog(){
        if(mProgressDialog !=null && mProgressDialog.isShowing()){
            mProgressDialog.dismiss();
        }
    }


    private void signIn(String email, String password){
        Log.i(TAG,"signIn");
        showProgressDialog();
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.i(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());
                        hideProgressDialog();
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if(task.isSuccessful()) {
                            Toast.makeText(FormActivity.this, "Autenticato!",
                                    Toast.LENGTH_SHORT).show();
                            Log.i(TAG,"Current user: " + mAuth.getCurrentUser().getUid());
                            startActivity(new Intent(FormActivity.this,MainActivity.class));
                            finish();
                        }
                        else {
                            Log.i(TAG, "signInWithEmail:failed", task.getException());
                            Toast.makeText(FormActivity.this, "Sign In failed",
                                    Toast.LENGTH_SHORT).show();

                        }


                    }
                });
    }


}


