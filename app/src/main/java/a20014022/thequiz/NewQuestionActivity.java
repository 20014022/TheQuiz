package a20014022.thequiz;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Ale on 06/02/2018.
 */

public class NewQuestionActivity extends AppCompatActivity {

    final String TAG = "QUESTIONI,OH";
    String prendoCategoria;
    String prendoTipo;
    DatabaseHelper dbHelper;
    Domanda domanda;
    private FirebaseDatabase database;// remote DB
    private DatabaseReference myRef; // reference to a location
    EditText testo;
    EditText nuova_categoria;
    RadioGroup radioLevelGroup;
    EditText opzioneA;
    EditText opzioneB;
    EditText opzioneC;
    EditText opzioneD;
    EditText risposta1;
    EditText risposta2;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
        prendoCategoria = getIntent().getStringExtra("Categoria");
        prendoTipo = getIntent().getStringExtra("Tipo");
        if (prendoTipo.equals("Open")) setContentView(R.layout.add_open_question);
        else setContentView(R.layout.add_closed_question);
        if (!prendoCategoria.equals("Altro")){
            LinearLayout cat = (LinearLayout)findViewById(R.id.categoria_layout);
            cat.setVisibility(View.GONE);
        }
        dbHelper = new DatabaseHelper(getApplicationContext());
        domanda = new Domanda();
        testo = (EditText)findViewById(R.id.edit_text);
        if (prendoCategoria.equals("Altro")) nuova_categoria = (EditText)findViewById(R.id.edit_text1);

        radioLevelGroup = (RadioGroup) findViewById(R.id.levelRadio);
        radioLevelGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int selectedId) {
                if(selectedId!=-1) {

                    //caso in cui il giocatore ha selezionato Facile
                    if(selectedId==R.id.radioEasy) {
                        domanda.setLivello(1);
                    }
                    //caso in cui il giocatore ha selezionato Media
                    if(selectedId==R.id.radioMedium) {
                        domanda.setLivello(2);
                    }
                    //caso in cui il giocatore ha selezionato Difficile
                    if(selectedId==R.id.radioHard) {
                        domanda.setLivello(3);
                    }
                }
            }
        });
        if (!prendoTipo.equals("Open")){
            opzioneA = (EditText)findViewById(R.id.edit_textA);
            opzioneB = (EditText)findViewById(R.id.edit_textB);
            opzioneC = (EditText)findViewById(R.id.edit_textC);
            opzioneD = (EditText)findViewById(R.id.edit_textD);
        }
        risposta1 = (EditText)findViewById(R.id.edit_textGiusta1);
        risposta2 = (EditText)findViewById(R.id.edit_textGiusta2);
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();
        if (!prendoCategoria.equals("Altro")) domanda.setCategoria(prendoCategoria);

    }

    private boolean validazione(){
        if(TextUtils.isEmpty(testo.getText().toString())){
            testo.setError("Required");
            return false;
        }else {
            domanda.setTesto(testo.getText().toString());
        }
        if (prendoCategoria.equals("Altro")){
            if(TextUtils.isEmpty(nuova_categoria.getText().toString())){
                nuova_categoria.setError("Required");
                return false;
            }else {
                domanda.setCategoria(nuova_categoria.getText().toString());
            }
        }
        Log.i(TAG,"Fino alla categoria");
        if (!prendoTipo.equals("Open")){
            String [] opzioni = new String[4];
            opzioni[0] = opzioneA.getText().toString();
            opzioni[1] = opzioneB.getText().toString();
            opzioni[2] = opzioneC.getText().toString();
            opzioni[3] = opzioneD.getText().toString();
            for(int i=0;i<4;i++){
                if(opzioni[i].isEmpty()){
                    return false;
                }
            }
            //controllo che non ci siano opzioni uguali
            for(int i=0;i<4;i++){
                Log.i(TAG,""+opzioni[i]);
                for(int j=1;j<4;j++){
                    if(i!=j){
                        if(opzioni[i].equals(opzioni[j])){
                            return false;
                        }
                    }
                }
            }
            domanda.setOpzioni(4,opzioni[0],opzioni[1],opzioni[2],opzioni[3]);
            Log.i(TAG,"Fino alle opzioni");
            if(TextUtils.isEmpty(risposta1.getText().toString())){
                risposta1.setError("Required");
                return false;
            }else {
                risposta1.setError(null);
            }
            boolean flag = false;
            String risposta = risposta1.getText().toString();
            Log.i(TAG,"Inizio a controllare la risposta con le opzioni");
            for(int i=0;i<4;i++){
                if(risposta.equals(opzioni[i])){
                    flag = true;
                    break;
                }
            }
            Log.i(TAG,"Fino alla risposta1");
            if(!flag) return false;
            if (prendoTipo.equals("Single choice")){
                domanda.setTipo(1);
                domanda.setRisposteGiuste(1,risposta);
            }
            else{
                //caso domanda multiple choice
                if(TextUtils.isEmpty(risposta2.getText().toString())){
                    risposta2.setError("Required");
                    return false;
                }else {
                    risposta2.setError(null);
                }
                flag = false;
                risposta = risposta2.getText().toString();
                for(int i=0;i<4;i++){
                    if(risposta.equals(opzioni[i])){
                        flag = true;
                        break;
                    }
                }
                if(!flag) return false;
                domanda.setTipo(2);
                domanda.setRisposteGiuste(2,risposta1.getText().toString(),risposta);
            }
        }
        else{
            //domanda open
            if(TextUtils.isEmpty(risposta1.getText().toString())){
                risposta1.setError("Required");
                return false;
            }else {
                risposta1.setError(null);
            }
            if(!TextUtils.isEmpty(risposta2.getText().toString())){
                domanda.setRisposteGiuste(2,risposta1.getText().toString(),risposta2.getText().toString());
            }
            else domanda.setRisposteGiuste(1,risposta1.getText().toString());
            domanda.setTipo(3);
        }
        domanda.setPicture(-1);
        if(domanda.getTipo()<1 || domanda.getTipo()>3) return false;
        return true;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.i(TAG,"onDestroy");
        dbHelper.close();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_question, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_save_question) {
            if(validazione()){
                Log.i(TAG,""+domanda.toString());
                dbHelper.addQuestion(domanda);
                myRef.child("newQuestions").push().setValue(domanda);
                Toast.makeText(this,getText(R.string.add_question_success),Toast.LENGTH_SHORT).show();
                finish();
            }
            else Toast.makeText(this,"Parametri errati.",Toast.LENGTH_SHORT).show();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }


}




