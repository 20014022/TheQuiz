package a20014022.thequiz;

/**
 * Created by Ale on 29/01/2018.
 */

public class SpinnerContainer {
    private String title;
    private boolean selected;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
