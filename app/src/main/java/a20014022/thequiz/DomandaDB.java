package a20014022.thequiz;

/**
 * Created by Ale on 28/01/2018.
 */

public class DomandaDB {

    private Domanda domanda;
    private int id;

    public Domanda getDomanda() {
        return domanda;
    }

    public void setDomanda(Domanda domanda) {
        this.domanda = domanda;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
