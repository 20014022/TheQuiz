package a20014022.thequiz;

/**
 * Created by Ale on 27/01/2018.
 */
import android.os.Bundle;
import android.preference.PreferenceActivity;

// Save preferences on DefaultSharedPreferences
public class EditPreferences extends PreferenceActivity {
    @SuppressWarnings("deprecation")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}
