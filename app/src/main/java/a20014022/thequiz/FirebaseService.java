package a20014022.thequiz;

/**
 * Created by Ale on 07/02/2018.
 */


import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class FirebaseService extends IntentService {
    static final String TAG = "ATISFIRE";
    private FirebaseDatabase database;// remote DB
    private DatabaseReference myRef; // reference to a location
    ValueEventListener myUserListener;
    DatabaseHelper dbHelper;
    int count = 0;

    public FirebaseService() {
        super("HelloIntentService");
        // Note: a toast here generates a NULL pointer exception
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i(TAG,"onHandleIntent");
        // pending activity referring to the MainActivity
        Intent notificationIntent = new Intent(this,MainActivity.class);
        notificationIntent.setAction(Intent.ACTION_MAIN);
        notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        final PendingIntent pi=PendingIntent.getActivity(this, 0,
                notificationIntent, 0);
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("newQuestions");
        Log.i(TAG,"database.getReference");
        dbHelper = new DatabaseHelper(getApplicationContext());
        /*
        // mandatory notification for foreground service
        final NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Notifica!")
                        .setAutoCancel(true)
                        .setContentText("Un servizio sta leggendo le domande dal database di Firebase")
                        .setContentIntent(pi);
        Notification notif= mBuilder.build();
        // go in foreground mode
        startForeground(777, notif);// on Android 7 this notification can be canceled like the others
*/
        String message = "Un servizio sta leggendo le domande dal database di Firebase";
        NotificationManager notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        Notification notif = mBuilder.setContentIntent(pi)
                .setSmallIcon(R.mipmap.ic_launcher).setTicker(getText(R.string.app_name)).setWhen(0)
                .setAutoCancel(true).setContentTitle("Notifica!")
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setContentText(message).build();
        notificationManager.notify(0,notif);

        // Read from the database
        myUserListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                Domanda domanda;
                for(DataSnapshot child : dataSnapshot.getChildren()){
                    domanda = child.getValue(Domanda.class);
                    Log.i(TAG,""+domanda.toString());
                    if(dbHelper.checkifNotinDatabase(domanda)){
                        dbHelper.addQuestion(domanda);
                        Log.i(TAG,"Aggiungo la domanda al database");
                        count++;
                    }
                }
                if(count>0){
                    Notification notif;
                    if(count>1){
                        mBuilder.setSmallIcon(android.R.drawable.ic_dialog_info)
                                .setContentTitle("Ho letto da Firebase")
                                .setContentText(count+" nuove domande aggiunte al database interno")
                                .setContentIntent(pi)
                                .setAutoCancel(true);
                        notif= mBuilder.build();
                    }
                    else{
                        mBuilder.setSmallIcon(android.R.drawable.ic_dialog_info)
                                .setContentTitle("Ho letto da Firebase")
                                .setContentText(count+" nuova domanda aggiunta al database interno")
                                .setContentIntent(pi)
                                .setAutoCancel(true);
                        notif= mBuilder.build();
                    }
                    // show notification
                    NotificationManager mNotificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    mNotificationManager.notify(20000 + (int) 1, notif);

                    count = 0;
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.i(TAG, "Failed to read value.", error.toException());
            }
        };
        Log.i(TAG,"Attach DatabaseReadListener");
        myRef.addValueEventListener(myUserListener);

        while(true);
        // remove from foreground
        //stopForeground(true);
        // Indent services do not have to be stopped explicitly!!!
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        dbHelper.close();
        myRef.removeEventListener(myUserListener);
        Log.i(TAG, "IntentService destroyed");
    }

}
