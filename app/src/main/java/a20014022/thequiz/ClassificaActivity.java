package a20014022.thequiz;

/**
 * Created by Ale on 04/02/2018.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ClassificaActivity extends Activity {

    ListView listView;
    private FirebaseDatabase database;// remote DB
    private DatabaseReference myRef; // reference to a location
    final private String TAG="CLASSIFICO";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.classifica);
        listView = (ListView)findViewById(R.id.listView);
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("users");
        Log.i(TAG,"Sto per interrogare il database");
        final ArrayList<User> utenti = new ArrayList<>();
        myRef.orderByChild("punteggio").limitToLast(10)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user;
                        for(DataSnapshot child : dataSnapshot.getChildren()){
                            user = child.getValue(User.class);
                            utenti.add(user);
                            Log.i(TAG,""+user.toString());
                        }
                        String[] arrayUtenti = new String[utenti.size()];
                        int indiceArray = 0;
                        for(int i=utenti.size()-1;i>=0;i--){
                            user = utenti.get(i);
                            arrayUtenti[indiceArray++] = (indiceArray)+".   "+user.getNick() + "   ->   "+user.getPunteggio()+" punti";
                        }
                        ArrayAdapter<String> itemsAdapter =
                                new ArrayAdapter<String>(ClassificaActivity.super.getApplicationContext(), android.R.layout.simple_list_item_1, arrayUtenti);
                        listView.setAdapter(itemsAdapter);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.i(TAG, "Failed to read value.", databaseError.toException());
                    }
                });

    }

    //metodo per gestire il clic da parte dell'utente del back button di android
    @Override
    public void onBackPressed(){
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }

}
