package a20014022.thequiz;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.ArrayList;


/**
 * Created by Ale on 25/01/2018.
 */

public class Domanda implements Parcelable{

    private String categoria;
    private int livello;
    private int tipo;
    private String testo;
    private ArrayList<String> opzioni;
    private ArrayList<String> risposteGiuste;
    final String TAG = "ROS";
    //tipo = 1 -> Single choice
    //tipo = 2 -> Multiple choice
    //tipo = 3 -> Open
    //livello = 1 -> basso
    //livello = 2 -> medio
    //livello = 3 -> difficile
    private int picture;

    public Domanda(){
        opzioni = new ArrayList<>();
        risposteGiuste = new ArrayList<>();
    }

    public Domanda(String categoria, int tipo, int livello){
        this.categoria = categoria;
        if(tipo>0 && tipo<4){
            this.tipo = tipo;
        }
        else{
            throw new IllegalArgumentException("Il tipo deve essere un numero compreso tra 0 e 3");
        }
        opzioni = new ArrayList<>();

        this.livello = livello;
        risposteGiuste = new ArrayList<>();
        picture = 0;
        Log.i(TAG,"Sono nel costruttore della domanda. Tipo = " + this.tipo);
    }


    /**
     * Use when reconstructing User object from parcel
     * This will be used only by the 'CREATOR'
     * @param in a parcel to read this object
     */
    public Domanda(Parcel in) {
        this.categoria = in.readString();
        this.livello = in.readInt();
        this.tipo = in.readInt();
        this.testo = in.readString();
        this.picture = in.readInt();
        this.opzioni = in.readArrayList(ArrayList.class.getClassLoader());
        this.risposteGiuste = in.readArrayList(ArrayList.class.getClassLoader());
        //in.readList(opzioni,List.class.getClassLoader());
        //in.readList(risposteGiuste,List.class.getClassLoader());
    }

    public void setTesto(String testo){
        this.testo = testo;
    }

    public void setRisposteGiuste(int k, String ...strings){
        risposteGiuste = new ArrayList<>();
        for(int i=0;i<k;i++){
            Log.i(TAG,strings[i]);
            risposteGiuste.add(strings[i]);
        }
    }

    public void setOpzioni(int k, String ...strings){
        //occhio che a domande Open non ha senso mettere delle opzioni
        opzioni = new ArrayList<>();
        Log.i(TAG,""+strings.length);
        for(int i=0;i<k;i++){
            //Log.i(TAG,strings[i]);
            opzioni.add(strings[i]);
        }
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public void setLivello(int livello) {
        this.livello = livello;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getCategoria() {
        return categoria;
    }

    public int getLivello() {
        return livello;
    }

    public int getTipo() {
        return tipo;
    }

    public String getTesto() {
        return testo;
    }

    public ArrayList<String> getOpzioni() {
        return opzioni;
    }

    public ArrayList<String> getRisposteGiuste() {
        return risposteGiuste;
    }

    @Override
    public String toString() {
        return "Domanda{" +
                "categoria='" + categoria + '\'' +
                ", livello=" + livello +
                ", tipo=" + tipo +
                ", testo='" + testo + '\'' +
                ", opzioni=" + opzioni +
                ", risposteGiuste=" + risposteGiuste +
                ", immagine=" + picture +
                '}';
    }

    public int getPicture() {
        return picture;
    }

    public void setPicture(int picture) {
        this.picture = picture;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Actual object serialization happens here, Write object content
     * to parcel, reading should be done according to this write order
     * param dest - parcel
     * param flags - Additional flags about how the object should be written
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(categoria);
        dest.writeInt(livello);
        dest.writeInt(tipo);
        dest.writeString(testo);
        dest.writeInt(picture);
        dest.writeList(opzioni);
        dest.writeList(risposteGiuste);
    }

    /**
     * This field is needed for Android to be able to
     * create new objects, individually or as arrays
     *
     * If you don’t do that, Android framework will through exception
     * Parcelable protocol requires a Parcelable.Creator object called CREATOR
     */
    public static final Parcelable.Creator<Domanda> CREATOR = new Parcelable.Creator<Domanda>() {

        public Domanda createFromParcel(Parcel in) {
            return new Domanda(in);
        }

        public Domanda[] newArray(int size) {
            return new Domanda[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Domanda domanda = (Domanda) o;

        if (livello != domanda.livello) return false;
        if (tipo != domanda.tipo) return false;
        if (picture != domanda.picture) return false;
        if (!categoria.equals(domanda.categoria)) return false;
        if (!testo.equals(domanda.testo)) return false;
        if (!opzioni.equals(domanda.opzioni)) return false;
        return risposteGiuste.equals(domanda.risposteGiuste);

    }

    @Override
    public int hashCode() {
        int result = categoria.hashCode();
        result = 31 * result + livello;
        result = 31 * result + tipo;
        result = 31 * result + testo.hashCode();
        result = 31 * result + opzioni.hashCode();
        result = 31 * result + risposteGiuste.hashCode();
        result = 31 * result + picture;
        return result;
    }
}


