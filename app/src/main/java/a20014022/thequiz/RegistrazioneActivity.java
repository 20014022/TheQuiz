package a20014022.thequiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Ale on 03/02/2018.
 */

public class RegistrazioneActivity extends FormActivity {

    Button submit;
    String auth;
    EditText rePasswd;
    EditText nickname;
    final String TAG = "AUTTREG";
    private FirebaseDatabase database;// remote DB
    private DatabaseReference myRef; // reference to a location
    private FirebaseAuth mAuth; // authorization
    Button clear;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.authform_registrazione);
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();
        mAuth = FirebaseAuth.getInstance();
        auth = "registrazione";
        eMail = (EditText) findViewById(R.id.edit_text);
        passwd = (EditText)findViewById(R.id.edit_text2);
        rePasswd = (EditText)findViewById(R.id.edit_textRepeat);
        nickname = (EditText)findViewById(R.id.edit_text3);
        submit = (Button)findViewById(R.id.submit_reg);
        submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.i(TAG,"email: "+ eMail.getText()+ " passwd: "+passwd.getText());
                if(validateForm()){
                    signUp(eMail.getText().toString(),passwd.getText().toString(),nickname.getText().toString());
                }
                else{
                    Toast.makeText(RegistrazioneActivity.this.getApplicationContext(),"Parametri errati",Toast.LENGTH_SHORT).show();
                }
            }
        });
        clear = (Button)findViewById(R.id.clear);
        clear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                eMail.setText("");
                passwd.setText("");
                rePasswd.setText("");
                nickname.setText("");
            }

        });
    }

    private void signUp(String email, String password, final String nickname){
        Log.i(TAG,"signUp");
        showProgressDialog();
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());
                        hideProgressDialog();
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Toast.makeText(RegistrazioneActivity.this, "Sign Up failed",
                                    Toast.LENGTH_SHORT).show();
                            Log.i(TAG,"signUp failed");
                        }
                        if(task.isSuccessful()){
                            Toast.makeText(RegistrazioneActivity.this, "Sei stato registrato",
                                    Toast.LENGTH_SHORT).show();
                            onAuthSuccess(task.getResult().getUser(),nickname);
                            Log.i(TAG,"Current user: " + mAuth.getCurrentUser().getUid());
                            startActivity(new Intent(RegistrazioneActivity.this,MainActivity.class));
                            finish();
                        }
                    }
                });

    }

    private String getUid(){
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    private void onAuthSuccess(FirebaseUser user, String nickname){
        writeNewUser(getUid(),nickname,user.getEmail());
    }

    private void writeNewUser(String userId, String name, String email){
        User user = new User(name,email,0);
        myRef.child("users").child(userId).setValue(user);

    }

}
