package a20014022.thequiz;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Button button;
    DatabaseHelper dbHelper;
    final String TAG = "SCOTTI";
    Domanda domanda;
    DomandaDB domDB;
    SharedPreferences prefs;
    float punteggio;
    float sommaPunteggiDomande = 0;
    boolean ultimaDomanda;
    int quanteDomande;
    int quante;
    Intent quizActivity;
    ArrayList<Integer> posizioniSoluzioni;
    ArrayList<Domanda> domandeQuiz;
    MyAdapter myAdapter;
    String whereClause;
    String[] whereArgs;
    ArrayList<String> listaCategorie;
    MyAdapter myAdapter2;
    ImageButton hourglass;
    int secondiRimanenti;
    Button score;
    float puntiRimanenti;
    private FirebaseDatabase database;// remote DB
    private DatabaseReference myRef; // reference to users
    private DatabaseReference myRefQuiz; // reference to quiz
    private FirebaseAuth mAuth; // authorization
    private FirebaseAuth.AuthStateListener mAuthListener;
    ValueEventListener myUserListener;
    boolean settoScore = false;
    float recordPrecedente;
    String ricordoCategoria = null;
    String ricordoTipo = null;
    AlertDialog alert;
    private ProgressDialog mProgressDialog;
    String keyQuiz;
    ValueEventListener newQuizListener;
    User Miouser = null;
    Quiz new_quiz = null;
    boolean trovato;
    boolean stoFacendoQuizScaricato;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);
        ImageView imv = (ImageView)findViewById(R.id.quiz_layout);
        imv.setImageResource(R.drawable.quiz_layout_principale);
        Log.i(TAG,"onCreate");
        // set default values for preference using the ones provided in the xml file
        // works only the first time the app is created
        PreferenceManager.setDefaultValues(this, R.xml.preferences,false);
        dbHelper = new DatabaseHelper(getApplicationContext());
        Log.i(TAG,"Istanziato dbHelper");
        aggiungiDomande();

        button = (Button)findViewById(R.id.new_quiz);
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                inizializzaQuiz();
                domDB = dbHelper.getRandomQuestion(posizioniSoluzioni);
                if(domDB==null){
                    //se domDB==null, devo terminare di proporre domande, anche se l'utente me ne ha chieste di più
                    ultimaDomanda(R.id.punteggio);
                    Toast.makeText(MainActivity.this.getApplicationContext() ,"Non ho altre domande del tipo richiesto nel database.",Toast.LENGTH_SHORT).show();
                }
                else {
                    domanda = domDB.getDomanda();
                    posizioniSoluzioni.add(domDB.getId());
                    domandeQuiz.add(domDB.getDomanda());
                    lanciaNuovaDomanda(0,-1,-3,domanda);
                }
            }
        });

        //adapter per spinner per i tipi di domande
        final String[] select_types = {""+getText(R.string.choose_type_of_question),
                "Single choice", "Multiple choice", "Open"};
        Spinner spinner = (Spinner) findViewById(R.id.spinner);

        ArrayList<SpinnerContainer> listTypes = new ArrayList<>();

        for (int i = 0; i < select_types.length; i++) {
            SpinnerContainer spCont = new SpinnerContainer();
            spCont.setTitle(select_types[i]);
            spCont.setSelected(false);
            listTypes.add(spCont);
        }
        myAdapter = new MyAdapter(MainActivity.this, 0,
                listTypes);
        spinner.setAdapter(myAdapter);



        hourglass = (ImageButton)findViewById(R.id.hourglass);
        hourglass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogTime();
            }
        });

        score = (Button)findViewById(R.id.score);
        score.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogScore();
            }
        });

        // initialization in onCreate()
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("users/"+mAuth.getCurrentUser().getUid());
        myRefQuiz = database.getReference("uploadedQuiz");
        Log.i(TAG,"database.getReference");
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.i(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.i(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };
        mAuth.addAuthStateListener(mAuthListener);

        // Read from the database
        myUserListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                Miouser = dataSnapshot.getValue(User.class);
                recordPrecedente = Miouser.getPunteggio();
                Log.i(TAG, "Value is: " + Miouser.toString());
                if(settoScore){
                    Log.i(TAG,"Sto guardando lo score.");
                    Log.i(TAG,"Punteggio ottenuto: "+punteggio+"/"+sommaPunteggiDomande);
                    float my_score = (float)(Math.pow(punteggio/sommaPunteggiDomande,2))*punteggio;
                    ultimaDomanda(R.id.punteggio);
                    Log.i(TAG, "Lo score è: " + my_score);
                    if(Miouser.getPunteggio()<my_score){
                        myRef.child("punteggio").setValue(my_score);
                        showDialogRecord(my_score);
                    }
                    settoScore = false;
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.i(TAG, "Failed to read value.", error.toException());
            }
        };
        Log.i(TAG,"Attach DatabaseReadListener");
        //starto il servizio che si occupa di leggere le nuove domande da Firebase
        //ed eventualmente caricarle nel database interno
        Intent tmp = new Intent(MainActivity.this, FirebaseService.class);
        startService(tmp);

        newQuizListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                Log.i(TAG,"Sto guardando se ci sono quiz nuovi");
                stoFacendoQuizScaricato = false;
                for(DataSnapshot child : dataSnapshot.getChildren()){
                    if(!stoFacendoQuizScaricato){
                        trovato = false;
                        Log.i(TAG,""+Miouser.toString());
                        new_quiz = child.getValue(Quiz.class);
                        Log.i(TAG,""+new_quiz.toString());
                        keyQuiz = child.getKey();
                        if(Miouser.getChiaviQuizFatti()!=null) {
                            for (int i = 0; i < Miouser.getChiaviQuizFatti().size(); i++) {
                                if (Miouser.getChiaviQuizFatti().get(i).equals(keyQuiz)) {
                                    trovato = true; //vuol dire che ho già fatto questo quiz
                                    Log.i(TAG,"Sono entrato nell'if. chiave = "+keyQuiz+"; chiave user: "+Miouser.getChiaviQuizFatti().get(i));
                                    //Log.i(TAG,"trovato = true; chiave = "+keyQuiz);
                                    break;
                                }
                            }
                        }
                        //se trovato = false, il quiz in questione non è ancora stato fatto
                        //mettere alertDialog che chiede se si vuole farlo
                        if(!trovato) {
                            Log.i(TAG,"trovato = false; chiave = "+keyQuiz);
                            stoFacendoQuizScaricato = true;
                            showDialogDownloadedQuiz();
                        }
                    }
                }
                //Log.i(TAG, "Value is: " + new_quiz.toString());
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.i(TAG, "Failed to read value.", error.toException());
            }
        };
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i(TAG,"onSaveInstanceState");
        TextView tv = (TextView) findViewById(R.id.punteggio);
        CharSequence punteggio_char = tv.getText();
        tv = (TextView) findViewById(R.id.punteggio_pers);
        CharSequence punteggio_pers = tv.getText();
        outState.putCharSequence("savedPunteggio", punteggio_char);
        outState.putCharSequence("savedPunteggioPers", punteggio_pers);
        if(domandeQuiz!=null) Log.i(TAG,""+domandeQuiz.toString());
        outState.putParcelableArrayList("ListaDomande",domandeQuiz);
        Button uploadQuiz = (Button)findViewById(R.id.upload_quiz);
        if((uploadQuiz.getVisibility()==View.VISIBLE) && (uploadQuiz.isClickable())) outState.putInt("condividi",1);
        else if((uploadQuiz.getVisibility()==View.VISIBLE) && (!uploadQuiz.isClickable())) outState.putInt("condividi",2);
        else outState.putInt("condividi",-1);   //se il bottone per condividere un quiz non è visibile, dovrà rimanerlo
        outState.putFloat("punteggio",this.punteggio);
        outState.putIntegerArrayList("posizioniSoluzioni",posizioniSoluzioni);
        outState.putFloat("puntiRimanenti",this.puntiRimanenti);
        outState.putInt("secondiRimanenti",this.secondiRimanenti);
        outState.putFloat("sommaPunteggiDomande",this.sommaPunteggiDomande);
        outState.putBoolean("ultimaDomanda",ultimaDomanda);
        outState.putParcelable("domanda",domanda);
        outState.putString("whereClause",whereClause);
        outState.putStringArray("whereArgs",whereArgs);
        if(new_quiz != null){
            ArrayList<Domanda> quiz = new_quiz.getQuiz();
            outState.putParcelableArrayList("new_quiz",quiz);
        }
        outState.putBoolean("settoScore",settoScore);
        outState.putParcelable("Miouser",Miouser);
        outState.putBoolean("stoFacendoQuizScaricato",stoFacendoQuizScaricato);
        outState.putString("keyQuiz",keyQuiz);
    }

    protected void onRestoreInstanceState(Bundle savedState) {
        super.onRestoreInstanceState(savedState);
        Log.i(TAG,"onRestoreInstanceState");
        TextView tv = (TextView) findViewById(R.id.punteggio);
        CharSequence punteggio_char = savedState.getCharSequence("savedPunteggio");
        tv.setText(punteggio_char);
        tv = (TextView) findViewById(R.id.punteggio_pers);
        CharSequence punteggio_pers = savedState.getCharSequence("savedPunteggioPers");
        tv.setText(punteggio_pers);
        this.domandeQuiz = savedState.getParcelableArrayList("ListaDomande");
        Button uploadQuiz = (Button)findViewById(R.id.upload_quiz);
        int upButton = savedState.getInt("condividi");
        if(upButton==1){
            uploadQuiz.setVisibility(View.VISIBLE);
            uploadQuiz.setClickable(true);
        }
        else if(upButton==2){
            uploadQuiz.setVisibility(View.VISIBLE);
            uploadQuiz.setClickable(false);
        }
        this.punteggio = savedState.getFloat("punteggio");
        this.posizioniSoluzioni = savedState.getIntegerArrayList("posizioniSoluzioni");
        this.puntiRimanenti = savedState.getFloat("puntiRimanenti");
        this.secondiRimanenti = savedState.getInt("secondiRimanenti");
        this.sommaPunteggiDomande = savedState.getFloat("sommaPunteggiDomande");
        this.ultimaDomanda = savedState.getBoolean("ultimaDomanda");
        this.domanda = savedState.getParcelable("domanda");
        this.whereClause = savedState.getString("whereClause");
        this.whereArgs = savedState.getStringArray("whereArgs");
        this.new_quiz = new Quiz();
        ArrayList<Domanda> quiz = savedState.getParcelableArrayList("new_quiz");
        Log.i(TAG,"parametri new quiz");
        this.new_quiz.setQuiz(quiz);
        this.settoScore = savedState.getBoolean("settoScore");
        this.Miouser = savedState.getParcelable("Miouser");
        this.stoFacendoQuizScaricato = savedState.getBoolean("stoFacendoQuizScaricato");
        this.keyQuiz = savedState.getString("keyQuiz");
        Log.i(TAG,"Ho recuperato Punteggio: "+ this.punteggio +" e sommaPunteggiDomande: "+sommaPunteggiDomande);
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG,"onPause");
        myRefQuiz.removeEventListener(newQuizListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG,"onResume");
        if(Miouser!=null) {
            Log.i(TAG,"onResume, MioUser è != null"+ Miouser.toString());
            myRefQuiz.addValueEventListener(newQuizListener);
        }
    }



    @Override
    public void onStart() {
        super.onStart();
        Log.i(TAG,"onStart");
        Log.i(TAG,"Current user: " + mAuth.getCurrentUser().getUid());
        myRef.addValueEventListener(myUserListener);

        //adapter per spinner per le categorie di domande
        //le categorie possono cambiare dinamicamente nella vita dell'app
        listaCategorie = dbHelper.dammiLeCategorie();
        final String[] categorie = new String[listaCategorie.size()+1];
        categorie[0] = getText(R.string.choose_cat_of_question).toString();
        for(int i=1;i<categorie.length;i++){
            categorie[i] = listaCategorie.get(i-1);
        }
        Spinner spinner2 = (Spinner) findViewById(R.id.spinner_categoria);
        ArrayList<SpinnerContainer> listTypes = new ArrayList<>();

        for (int i = 0; i < categorie.length; i++) {
            SpinnerContainer spCont = new SpinnerContainer();
            spCont.setTitle(categorie[i]);
            spCont.setSelected(false);
            listTypes.add(spCont);
        }
        myAdapter2 = new MyAdapter(MainActivity.this, 0,
                listTypes);
        spinner2.setAdapter(myAdapter2);

    }

    @Override
    public void onStop() {
        super.onStop();
        myRef.removeEventListener(myUserListener);
    }


    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.i(TAG,"onDestroy");
        dbHelper.close();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }


    private void showDialogRecord(float score){
        suono();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder
                .setTitle(getText(R.string.new_record))
                .setCancelable(false)
                .setIcon(android.R.drawable.btn_star)
                .setMessage("Hai ottenuto uno score di "+score+" punti!")
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.cancel();
                            }
                        })
                .setNegativeButton(getText(R.string.goto_classifica), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Log.i(TAG,"Sto per andare alla classifica");
                        Intent goto_classifica = new Intent(getApplicationContext(), ClassificaActivity.class);
                        startActivityForResult(goto_classifica,4);
                        //startActivity(new Intent(MainActivity.this,ClassificaActivity.class));
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void showDialogTime(){
        final View addView=getLayoutInflater().inflate(R.layout.dialog, null);
        Log.i(TAG,"inflate");
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder
                .setTitle(getText(R.string.time_limit))
                .setCancelable(false)
                .setMessage("Record precedente: "+recordPrecedente+" punti")
                .setView(addView)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                EditText e=(EditText)addView.findViewById(R.id.edit_text);
                                String t = e.getText().toString();
                                if(t.charAt(0)=='0'){
                                    Toast.makeText(MainActivity.this.getApplicationContext() ,"Devi inserire un intero tra 1 e 9",Toast.LENGTH_SHORT).show();
                                }
                                else{
                                    int n = Integer.parseInt(t);
                                    secondiRimanenti = n * 60;
                                    inizializzaQuiz();
                                    domDB = dbHelper.getRandomQuestion(posizioniSoluzioni);
                                    if(domDB==null){
                                        //se domDB==null, devo terminare di proporre domande, anche se l'utente me ne ha chieste di più
                                        ultimaDomanda(R.id.punteggio);
                                        Toast.makeText(MainActivity.this.getApplicationContext() ,"Non ho altre domande del tipo richiesto nel database.",Toast.LENGTH_SHORT).show();
                                    }
                                    else {
                                        domanda = domDB.getDomanda();
                                        posizioniSoluzioni.add(domDB.getId());
                                        domandeQuiz.add(domDB.getDomanda());
                                        lanciaNuovaDomanda(0,secondiRimanenti,-3,domanda);
                                    }
                                }

                            }
                        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void showDialogScore(){
        final View addView=getLayoutInflater().inflate(R.layout.dialog_score, null);
        Log.i(TAG,"inflate");
        new AlertDialog.Builder(this)
                .setTitle(getText(R.string.score_limit))
                .setView(addView)
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                EditText e=(EditText)addView.findViewById(R.id.edit_text);
                                String t = e.getText().toString();
                                if(t.charAt(0)=='0'){
                                    Toast.makeText(MainActivity.this.getApplicationContext() ,"Devi inserire un intero tra 1 e 100",Toast.LENGTH_SHORT).show();
                                }
                                else{
                                    puntiRimanenti = Integer.parseInt(t);
                                    inizializzaQuiz();
                                    domDB = dbHelper.getRandomQuestion(posizioniSoluzioni);
                                    if(domDB==null){
                                        //se domDB==null, devo terminare di proporre domande, anche se l'utente me ne ha chieste di più
                                        ultimaDomanda(R.id.punteggio);
                                        Toast.makeText(MainActivity.this.getApplicationContext() ,"Non ho altre domande del tipo richiesto nel database.",Toast.LENGTH_SHORT).show();
                                    }
                                    else {
                                        domanda = domDB.getDomanda();
                                        posizioniSoluzioni.add(domDB.getId());
                                        domandeQuiz.add(domDB.getDomanda());
                                        lanciaNuovaDomanda(2,-1,puntiRimanenti,domanda);
                                    }
                                }

                            }
                        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                    }
                })
                .show();

    }

    private void showDialogChooseCategory(){
        listaCategorie = dbHelper.dammiLeCategorie();
        Log.i(TAG,"1");
        final String[] categorie = new String[listaCategorie.size()+1];
        int i;
        for(i=0;i<listaCategorie.size();i++){
            categorie[i] = listaCategorie.get(i);
            Log.i(TAG,""+listaCategorie.get(i));
        }
        Log.i(TAG,"2");
        categorie[i] = "Altro";
        AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
        Log.i(TAG,"3");
        alt_bld.setTitle(R.string.choose_cat_of_question);
        alt_bld.setCancelable(true);
        Log.i(TAG,"4");
        alt_bld.setSingleChoiceItems(categorie, -1, new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int item){
                ricordoCategoria = categorie[item];
                Log.i(TAG,""+ricordoCategoria);
                dialog.dismiss();
                if(ricordoTipo!=null && ricordoCategoria!=null){
                    Log.i(TAG,"5");
                    Intent newQuestion = new Intent(getApplicationContext(),NewQuestionActivity.class);
                    newQuestion.putExtra("Categoria",ricordoCategoria);
                    newQuestion.putExtra("Tipo",ricordoTipo);
                    startActivity(newQuestion);
                }
            }
        });
        alert = alt_bld.create();
        //alert.show();
    }

    private void showDialogChooseType(){
        final CharSequence[] select_types = {"Single choice", "Multiple choice", "Open"};
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>
                (this,android.R.layout.simple_list_item_single_choice,select_types);
        showDialogChooseCategory();
        Log.i(TAG,"Buildato l'alert per le categorie");
        AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
        alt_bld.setTitle(R.string.choose_type_of_question);
        alt_bld.setSingleChoiceItems(adapter, -1, new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int item){
                switch(item){
                    case 0: ricordoTipo = "Single choice";
                        break;
                    case 1: ricordoTipo = "Multiple choice";
                        break;
                    case 2: ricordoTipo = "Open";
                        break;
                    default:
                        break;
                }
                Log.i(TAG,""+ricordoTipo);
                dialog.dismiss();
                alert.show();
            }
        });
        AlertDialog alert2 = alt_bld.create();
        alert2.show();
    }

    private void showDialogDownloadedQuiz(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder
                .setTitle(getText(R.string.new_quiz_available))
                .setMessage(getText(R.string.try_it))
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                                DoDownloadedQuiz();
                            }
                        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                    }
                })
                .setNeutralButton("Non mi interessa", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                        Miouser.addKeytoArrayList(keyQuiz);
                        Log.i(TAG,"Attualmente user è:"+Miouser.toString());
                        myRef.setValue(Miouser);
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void DoDownloadedQuiz(){
        inizializzaQuiz();
        quante = new_quiz.getQuiz().size();
        domanda = new_quiz.getQuiz().get(quanteDomande);
        lanciaNuovaDomanda(3,-1,-3,domanda);
    }

    private void inizializzaQuiz(){
        domandeQuiz = new ArrayList<>();
        posizioniSoluzioni = new ArrayList<>();
        punteggio = 0;
        quanteDomande = 0;
        Log.i(TAG,"onClick bottone del new quiz");
        quante = prendiDaPreferenze();
        nascondiBottoneUpload();
    }

    private void lanciaNuovaDomanda(int requestCode, int secondi, float puntiRimanenti, final Domanda mydomanda){
        Log.i(TAG,"Sto per far partire la quizActivity");
        Log.i(TAG,"posizioniSoluzioni: "+posizioniSoluzioni.toString());

        quizActivity = new Intent(getApplicationContext(), NewQuiz.class);
        quizActivity.putExtra("NuovaDomanda",mydomanda);
        quizActivity.putExtra("Secondi",secondi);
        quizActivity.putExtra("Punti",puntiRimanenti);
        Log.i(TAG,"Sono in lanciaNuovaDomanda. "+"quanteDomande: "+quanteDomande+"; quante: "+quante);
        quizActivity.putExtra("quanteDomande",quanteDomande);
        quizActivity.putExtra("quante",quante);
        if(quanteDomande==quante-1){
            quizActivity.putExtra("Ultima",true);
        }
        sommaPunteggiDomande += mydomanda.getLivello();
        startActivityForResult(quizActivity,requestCode);
        Log.i(TAG,"Ho lanciato la nuova domanda.");

    }

    private Integer prendiDaPreferenze(){
        prefs=PreferenceManager.getDefaultSharedPreferences(this);
        return Integer.parseInt(prefs.getString("how_many_questions_for_quiz","5"));
    }

    public void onClickTipoQuizPersonalizzato(View v){
        boolean tipo1 = myAdapter.getItem(1).isSelected();
        boolean tipo2 = myAdapter.getItem(2).isSelected();
        boolean tipo3 = myAdapter.getItem(3).isSelected();
        if(!tipo1 && !tipo2 && !tipo3){
            Toast.makeText(this,"Seleziona prima un tipo",Toast.LENGTH_SHORT).show();
        }
        else if((tipo1 && !tipo2 && !tipo3) || (!tipo1 && tipo2 && !tipo3) || (!tipo1 && !tipo2 && tipo3)){
            //Ho selezionato un solo tipo di domande
            whereClause = " WHERE tipo = ? ";
            whereArgs = new String[1];
            if(tipo1) {
                whereArgs[0] = "1";
            }
            else if(tipo2) {
                whereArgs[0] = "2";
            }
            else {
                whereArgs[0] = "3";
            }
            inizializzaQuiz();
            domDB = dbHelper.getSpecificQuestion(posizioniSoluzioni,whereClause,whereArgs);
            if(domDB==null){
                //se domDB==null, devo terminare di proporre domande, anche se l'utente me ne ha chieste di più
                ultimaDomanda(R.id.punteggio_pers);
                Toast.makeText(this,"Non ho altre domande del tipo richiesto nel database.",Toast.LENGTH_SHORT).show();
            }
            else {
                domanda = domDB.getDomanda();
                posizioniSoluzioni.add(domDB.getId());
                domandeQuiz.add(domDB.getDomanda());
                lanciaNuovaDomanda(1,-1,-3,domanda);
            }
        }
        else if((tipo1 && tipo2 && !tipo3) || (!tipo1 && tipo2 && tipo3) || (tipo1 && !tipo2 && tipo3)){
            //Ho selezionato due tipi di domande
            whereClause = " WHERE tipo = ? OR tipo = ?";
            whereArgs = new String[2];
            if(!tipo3) {
                whereArgs[0] = "1";
                whereArgs[1] = "2";
            }
            else if(!tipo1) {
                whereArgs[0] = "2";
                whereArgs[1] = "3";
            }
            else {
                whereArgs[0] = "1";
                whereArgs[1] = "3";
            }
            inizializzaQuiz();
            domDB = dbHelper.getSpecificQuestion(posizioniSoluzioni,whereClause,whereArgs);
            if(domDB==null){
                //se domDB==null, devo terminare di proporre domande, anche se l'utente me ne ha chieste di più
                ultimaDomanda(R.id.punteggio_pers);
                Toast.makeText(this,"Non ho altre domande del tipo richiesto nel database.",Toast.LENGTH_SHORT).show();
            }
            else {
                domanda = domDB.getDomanda();
                posizioniSoluzioni.add(domDB.getId());
                domandeQuiz.add(domDB.getDomanda());
                lanciaNuovaDomanda(1,-1,-3,domanda);
            }
        }
        else{
            //Ho selezionato tutti i tipi: è un altro modo per dire: voglio una domanda casuale!
            inizializzaQuiz();
            domDB = dbHelper.getRandomQuestion(posizioniSoluzioni);
            if(domDB==null){
                //se domDB==null, devo terminare di proporre domande, anche se l'utente me ne ha chieste di più
                ultimaDomanda(R.id.punteggio);
                Toast.makeText(this,"Non ho altre domande del tipo richiesto nel database.",Toast.LENGTH_SHORT).show();
            }
            else {
                domanda = domDB.getDomanda();
                posizioniSoluzioni.add(domDB.getId());
                domandeQuiz.add(domDB.getDomanda());
                lanciaNuovaDomanda(0,-1,-3,domanda);
            }
        }

    }

    public void onClickCategoriaQuizPersonalizzato(View v){
        boolean trovatoAlmenoUnaCategoria = false;
        ArrayList<String> listaArgomenti = new ArrayList<>();
        for(int i=1;i<myAdapter2.getCount();i++){
            if(myAdapter2.getItem(i).isSelected() && trovatoAlmenoUnaCategoria){
                whereClause = whereClause + " OR cat = ?";
                listaArgomenti.add(myAdapter2.getItem(i).getTitle());
                Log.i(TAG,"getTitle: "+myAdapter2.getItem(i).getTitle());
            }
            else if(myAdapter2.getItem(i).isSelected() && !trovatoAlmenoUnaCategoria){
                whereClause = " WHERE cat = ?";
                listaArgomenti.add(myAdapter2.getItem(i).getTitle());
                Log.i(TAG,"getTitle: "+myAdapter2.getItem(i).getTitle());
                trovatoAlmenoUnaCategoria = true;
            }
        }
        if(listaArgomenti.isEmpty()) Toast.makeText(this,"Seleziona prima un tipo",Toast.LENGTH_SHORT).show();
        else{
            whereArgs = new String[listaArgomenti.size()];
            for(int i=0;i<whereArgs.length;i++){
                whereArgs[i] = listaArgomenti.get(i);
            }
            Log.i(TAG,"whereClause: "+whereClause.toString()+"; whereArgs: "+whereArgs.toString());
            if(whereArgs.length<listaCategorie.size()){
                //ho selezionato qualcuna delle opzioni
                inizializzaQuiz();
                domDB = dbHelper.getSpecificQuestion(posizioniSoluzioni,whereClause,whereArgs);
                if(domDB==null){
                    //se domDB==null, devo terminare di proporre domande, anche se l'utente me ne ha chieste di più
                    ultimaDomanda(R.id.punteggio_pers);
                    Toast.makeText(this,"Non ho altre domande del tipo richiesto nel database.",Toast.LENGTH_SHORT).show();
                }
                else {
                    domanda = domDB.getDomanda();
                    posizioniSoluzioni.add(domDB.getId());
                    domandeQuiz.add(domDB.getDomanda());
                    lanciaNuovaDomanda(1,-1,-3,domanda);
                }
            }
            else{
                //caso in cui ho selezionato tutte le opzioni -> quiz random!
                inizializzaQuiz();
                domDB = dbHelper.getRandomQuestion(posizioniSoluzioni);
                if(domDB==null){
                    //se domDB==null, devo terminare di proporre domande, anche se l'utente me ne ha chieste di più
                    ultimaDomanda(R.id.punteggio);
                    Toast.makeText(this,"Non ho altre domande del tipo richiesto nel database.",Toast.LENGTH_SHORT).show();
                }
                else {
                    domanda = domDB.getDomanda();
                    posizioniSoluzioni.add(domDB.getId());
                    domandeQuiz.add(domDB.getDomanda());
                    lanciaNuovaDomanda(0,-1,-3,domanda);
                }
            }
        }
    }

    private void ultimaDomanda(int testoPunteggioID){
        Log.i(TAG,"Sono entrato nell'ultima domanda");
        TextView tv = (TextView)findViewById(testoPunteggioID);
        tv.setText("Punteggio ottenuto: "+punteggio+"/"+sommaPunteggiDomande);
        sommaPunteggiDomande = 0;
        punteggio = 0;
        final Button uploadQuiz = (Button)findViewById(R.id.upload_quiz);
        if(!stoFacendoQuizScaricato){
            uploadQuiz.setVisibility(View.VISIBLE);
            uploadQuiz.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (domandeQuiz.size()>0){
                        Quiz nuovoQuiz = new Quiz();
                        nuovoQuiz.setQuiz(domandeQuiz);
                        keyQuiz = myRefQuiz.push().getKey();
                        Log.i(TAG,"chiave del quiz caricato: "+keyQuiz);
                        myRefQuiz.child(keyQuiz).setValue(nuovoQuiz);
                        Miouser.addKeytoArrayList(keyQuiz);
                        Log.i(TAG,"Attualmente user è:"+Miouser.toString());
                        myRef.setValue(Miouser);
                        uploadQuiz.setClickable(false);
                        Toast.makeText(MainActivity.this.getApplicationContext(),"Condiviso il quiz con successo!",Toast.LENGTH_SHORT).show();
                    }
                    else {
                        uploadQuiz.setVisibility(View.GONE);
                        Log.i(TAG,"L'arrayList di domande è vuoto");
                    }
                }
            });
        }

    }

    private void nascondiBottoneUpload(){
        Button uploadQuiz = (Button)findViewById(R.id.upload_quiz);
        uploadQuiz.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(!(resultCode == Activity.RESULT_CANCELED)) {
            if (requestCode == 0) {
                Log.i(TAG, "requestCode = 0");
                float punteggioDomanda;
                punteggioDomanda = data.getFloatExtra("result", 0);
                //Toast.makeText(this, "Punteggio: "+punteggio, Toast.LENGTH_SHORT).show();
                if (punteggioDomanda != -1) punteggio += punteggioDomanda;
                quanteDomande = data.getIntExtra("quanteDomande", quanteDomande);
                quante = data.getIntExtra("quante", quante);
                ultimaDomanda = data.getBooleanExtra("Ultima", false);
                secondiRimanenti = data.getIntExtra("Secondi", -1);
                Log.i(TAG, "secondiRimanenti = " + secondiRimanenti);
                if (secondiRimanenti != -1) {
                    if (secondiRimanenti == -2) {
                        settoScore = true;
                        //si è raggiunto il tempo limite senza che l'utente abbia potuto dare una risposta alla domanda corrente
                        //caso in cui l'utente non ha avuto il tempo per rispondere alla domanda
                        if (punteggioDomanda == -1) sommaPunteggiDomande -= domanda.getLivello();
                        if (settoScore)
                            myRef.child("email").setValue(mAuth.getCurrentUser().getEmail());
                        else ultimaDomanda(R.id.punteggio);
                    } else {
                        //secondiRimanenti>=0
                        if (secondiRimanenti <= 1) {
                            //non c'è tempo per rispondere a un'altra domanda: guardo il punteggio che ho ottenuto
                            //ultimaDomanda(R.id.punteggio);
                            settoScore = true;
                            myRef.child("email").setValue(mAuth.getCurrentUser().getEmail());

                        } else {
                            domDB = dbHelper.getRandomQuestion(posizioniSoluzioni);
                            if (domDB == null) {
                                //se domDB==null, devo terminare di proporre domande, anche se c'è ancora tempo per farne altre
                                //ultimaDomanda(R.id.punteggio);
                                settoScore = true;
                                myRef.child("email").setValue(mAuth.getCurrentUser().getEmail());
                                Toast.makeText(this, "Non ho altre domande del tipo richiesto nel database.", Toast.LENGTH_SHORT).show();
                            } else {
                                domanda = domDB.getDomanda();
                                posizioniSoluzioni.add(domDB.getId());
                                domandeQuiz.add(domDB.getDomanda());
                                lanciaNuovaDomanda(0, secondiRimanenti, -3, domanda);
                            }
                        }
                    }
                } else {
                    quanteDomande++;
                    Log.i(TAG, "da qualche parte; " + ultimaDomanda + "; quanteDomande: " + quanteDomande + "; quante: " + quante);
                    Log.i(TAG, "" + posizioniSoluzioni.toString());
                    Log.i(TAG, "" + domandeQuiz.toString());
                    if (ultimaDomanda) ultimaDomanda(R.id.punteggio);
                    else if (quanteDomande < quante) {
                        domDB = dbHelper.getRandomQuestion(posizioniSoluzioni);
                        Log.i(TAG, "Niente creash in dbHelper: ");
                        if (domDB == null) {
                            //se domDB==null, devo terminare di proporre domande, anche se l'utente me ne ha chieste di più
                            ultimaDomanda(R.id.punteggio);
                            Toast.makeText(this, "Non ho altre domande del tipo richiesto nel database.", Toast.LENGTH_SHORT).show();
                        } else {
                            domanda = domDB.getDomanda();
                            Log.i(TAG, "Ho pescato questa domanda: " + domanda.toString());
                            posizioniSoluzioni.add(domDB.getId());
                            domandeQuiz.add(domDB.getDomanda());
                            Log.i(TAG, "Sto lanciando una nuova domanda");
                            lanciaNuovaDomanda(0, -1, -3, domanda);
                        }
                    }
                }
            } else if (requestCode == 1) {
                Log.i(TAG, "requestCode = 1");
                float punteggioDomanda;
                punteggioDomanda = data.getFloatExtra("result", 0);
                //Toast.makeText(this, "Punteggio: "+punteggio, Toast.LENGTH_SHORT).show();
                quanteDomande = data.getIntExtra("quanteDomande", quanteDomande);
                quante = data.getIntExtra("quante", quante);
                punteggio += punteggioDomanda;
                ultimaDomanda = data.getBooleanExtra("Ultima", false);
                quanteDomande++;
                Log.i(TAG, "da qualche parte; " + ultimaDomanda + "; quanteDomande: " + quanteDomande + "; quante: " + quante);
                Log.i(TAG, "" + posizioniSoluzioni.toString());
                Log.i(TAG, "" + domandeQuiz.toString());
                if (ultimaDomanda) ultimaDomanda(R.id.punteggio_pers);
                else if (quanteDomande < quante) {
                    domDB = dbHelper.getSpecificQuestion(posizioniSoluzioni, whereClause, whereArgs);
                    Log.i(TAG, "Niente creash in dbHelper: ");
                    if (domDB == null) {
                        //se domDB==null, devo terminare di proporre domande, anche se l'utente me ne ha chieste di più
                        ultimaDomanda(R.id.punteggio_pers);
                        Toast.makeText(this, "Non ho altre domande del tipo richiesto nel database.", Toast.LENGTH_SHORT).show();
                    } else {
                        domanda = domDB.getDomanda();
                        posizioniSoluzioni.add(domDB.getId());
                        domandeQuiz.add(domDB.getDomanda());
                        lanciaNuovaDomanda(1, -1, -3, domanda);
                    }
                }
            } else if (requestCode == 2) {
                //requestCode == 2. L'utente sta facendo una score limit
                Log.i(TAG, "requestCode = 2");
                float punteggioDomanda;
                punteggioDomanda = data.getFloatExtra("result", 0);
                punteggio += punteggioDomanda;
                puntiRimanenti = data.getFloatExtra("Punti", -3);
                quanteDomande = data.getIntExtra("quanteDomande", quanteDomande);
                quante = data.getIntExtra("quante", quante);
                if (puntiRimanenti > 0) {
                    domDB = dbHelper.getRandomQuestion(posizioniSoluzioni);
                    if (domDB == null) {
                        //se domDB==null, devo terminare di proporre domande, anche se l'utente me ne ha chieste di più
                        ultimaDomanda(R.id.punteggio);
                        Toast.makeText(this, "Non ho altre domande del tipo richiesto nel database.", Toast.LENGTH_SHORT).show();
                    } else {
                        domanda = domDB.getDomanda();
                        posizioniSoluzioni.add(domDB.getId());
                        domandeQuiz.add(domDB.getDomanda());
                        lanciaNuovaDomanda(2, -1, puntiRimanenti, domanda);
                    }
                } else {
                    //l'utente ha raggiunto i punti che si era fissato
                    ultimaDomanda(R.id.punteggio);
                }
            } else if (requestCode == 3) {
                //requestCode == 3. L'utente sta rispondendo a un quiz scaricato da firebase
                Log.i(TAG, "requestCode = 3");
                float punteggioDomanda;
                punteggioDomanda = data.getFloatExtra("result", 0);
                //Toast.makeText(this, "Punteggio: "+punteggio, Toast.LENGTH_SHORT).show();
                punteggio += punteggioDomanda;
                quanteDomande = data.getIntExtra("quanteDomande", quanteDomande);
                quante = data.getIntExtra("quante", quante);
                ultimaDomanda = data.getBooleanExtra("Ultima", false);
                quanteDomande++;
                if (ultimaDomanda) {
                    Miouser.addKeytoArrayList(keyQuiz);
                    Log.i(TAG, "(in onActivityResult) Attualmente user è:" + Miouser.toString() + " e keyQuiz: " + keyQuiz);
                    myRef.setValue(Miouser);
                    ultimaDomanda(R.id.punteggio_pers);
                } else if (quanteDomande < quante) {
                    domanda = new_quiz.getQuiz().get(quanteDomande);
                    lanciaNuovaDomanda(3, -1, -3, domanda);
                }
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_add_question) {
            Log.i(TAG,"showDialogType");
            showDialogChooseType();
            return true;
        }
        if (id == R.id.action_sign_out) {
            mAuth.signOut();
            startActivity(new Intent(this, FormActivity.class));
            return true;
        }
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, EditPreferences.class));
            return true;
        }
        if (id == R.id.action_info) {
            startActivity(new Intent(this, InfoActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    void suono(){
        MediaPlayer mp;
        mp = MediaPlayer.create(getApplicationContext(), R.raw.trumpet);
        mp.start();
    }

    protected void showProgressDialog(){
        if(mProgressDialog == null){
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Solo un secondo...");
        }
        mProgressDialog.show();
    }

    protected void hideProgressDialog(){
        if(mProgressDialog !=null && mProgressDialog.isShowing()){
            mProgressDialog.dismiss();
        }
    }

    void aggiungiDomande(){
        showProgressDialog();
        ArrayList<Domanda> contenitore = new ArrayList<>();
        //public Domanda(String categoria, int tipo, int livello)
        Domanda dom = new Domanda("Geografia",1,2);
        dom.setTesto("Qual è lo Stato con la popolazione ispanofona più numerosa?");
        dom.setOpzioni(4,"Il Messico","La Colombia","L'Argentina","La Spagna");
        dom.setRisposteGiuste(1,"Il Messico");
        contenitore.add(dom);
        dom = new Domanda("Grammatica",1,1);
        dom.setTesto("Il passato remoto di \"vado\" è:");
        dom.setOpzioni(4,"Andai","Andavo","Sono andato","Ero andato");
        dom.setRisposteGiuste(1,"Andai");
        contenitore.add(dom);
        dom = new Domanda("Grammatica",1,1);
        dom.setTesto("Qual è il plurale di \"capolavoro\"?");
        dom.setOpzioni(4,"Capolavoro","Capolavori","Capilavoro","Capilavori");
        dom.setRisposteGiuste(1,"Capolavori");
        contenitore.add(dom);
        dom = new Domanda("Geografia",1,1);
        dom.setTesto("Quale tra queste città italiane si trova più a nord?");
        dom.setOpzioni(4,"Matera","Macerata","Arezzo","Gorizia");
        dom.setRisposteGiuste(1,"Gorizia");
        contenitore.add(dom);
        dom = new Domanda("Geografia",1,2);
        dom.setTesto("Quale tra questi monumenti è il più alto?");
        dom.setOpzioni(4,"Torre Eiffel","Mole Antonelliana","Big Ben","Torre di Pisa");
        dom.setRisposteGiuste(1,"Torre Eiffel");
        contenitore.add(dom);
        dom = new Domanda("Geografia",1,2);
        dom.setTesto("Il Canale di Panama mette in comunicazione tra loro l'Oceano Atlantico e l'Oceano...");
        dom.setOpzioni(4,"Indiano","Artico","Pacifico","Antartico");
        dom.setRisposteGiuste(1,"Pacifico");
        contenitore.add(dom);
        dom = new Domanda("Arte&Letteratura",1,3);
        dom.setTesto("Quale città veneta, patrimonio dell'UNESCO, è detta anche \"città del Palladio?\"");
        dom.setOpzioni(4,"Rovigo","Venezia","Vicenza","Belluno");
        dom.setRisposteGiuste(1,"Vicenza");
        contenitore.add(dom);
        dom = new Domanda("Geografia",1,1);
        dom.setTesto("Quale città è detta \"la ville lumière\", la città delle luci?");
        dom.setOpzioni(4,"Parigi","Berlino","Milano","Madrid");
        dom.setRisposteGiuste(1,"Parigi");
        contenitore.add(dom);
        dom = new Domanda("Geografia",1,2);
        dom.setTesto("Quale fiume bagna la città di Budapest?");
        dom.setOpzioni(4,"Danubio","Moldava","Reno","Volga");
        dom.setRisposteGiuste(1,"Danubio");
        dom.setPicture(R.drawable.budapest);
        contenitore.add(dom);
        dom = new Domanda("Geografia",1,2);
        dom.setTesto("Qual è la capitale del Liechtenstein?");
        dom.setOpzioni(4,"Liechtenstein","La Valletta","Sofia","Vaduz");
        dom.setRisposteGiuste(1,"Vaduz");
        dom.setPicture(R.drawable.liechtenstein);
        contenitore.add(dom);
        dom = new Domanda("Scienze&affini",3,1);
        dom.setTesto("Quale numero segue quelli riportati di seguito secondo la successione di Fibonacci? 5,8,13,21,...");
        dom.setRisposteGiuste(1,"34");
        contenitore.add(dom);
        dom = new Domanda("Scienze&affini",1,2);
        dom.setTesto("L'herpes è un'infezione provocata da un:");
        dom.setOpzioni(4,"Virus","Batterio","Fungo","Verme");
        dom.setRisposteGiuste(1,"Virus");
        contenitore.add(dom);
        dom = new Domanda("Geografia",1,2);
        dom.setTesto("Una sottile lingua di terra bagnata da due lati del mare è?");
        dom.setOpzioni(4,"Un atollo","Una penisola","Un istmo","Uno stretto");
        dom.setRisposteGiuste(1,"Un istmo");
        contenitore.add(dom);
        dom = new Domanda("Scienze&affini",2,2);
        dom.setTesto("Quali delle seguenti affermazioni sul pinguino sono vere?");
        dom.setOpzioni(4,"Non ha nemici naturali","E' un mammifero","Depone uova","Vive al Polo Sud");
        dom.setRisposteGiuste(2,"Depone uova","Vive al Polo Sud");
        dom.setPicture(R.drawable.pinguino);
        contenitore.add(dom);
        dom = new Domanda("Scienze&affini",3,2);
        dom.setTesto("Qual è il simbolo chimico del potassio?");
        dom.setRisposteGiuste(1,"K");
        contenitore.add(dom);
        dom = new Domanda("Scienze&affini",3,1);
        dom.setTesto("Qual è l'unità di misura usata per calcolare le enormi distanze tra due corpi celesti? (omettere l'articolo)");
        dom.setRisposteGiuste(2,"Anno luce","anno luce");
        contenitore.add(dom);
        dom = new Domanda("Scienze&affini",3,1);
        dom.setTesto("Qual è il pianeta del Sistema Solare famoso per i propri anelli?");
        dom.setRisposteGiuste(1,"Saturno");
        dom.setPicture(R.drawable.saturnx);
        contenitore.add(dom);
        dom = new Domanda("Fun",1,1);
        dom.setTesto("Che animale è il simpatico protagonista del film d'animazione \"Ratatouille\"?");
        dom.setOpzioni(4,"Un gatto","Un pinguino","Un topo","Uno scoiattolo");
        dom.setRisposteGiuste(1,"Un topo");
        contenitore.add(dom);

        dom = new Domanda("Grammatica",2,1);
        dom.setTesto("Quali tra queste espressioni sono scritte correttamente?");
        dom.setOpzioni(4,"Fedifrago","Calvizia","Propio","Un'amica");
        dom.setRisposteGiuste(2,"Fedifrago","Un'amica");
        contenitore.add(dom);
        dom = new Domanda("Grammatica",1,2);
        dom.setTesto("Qual è la grafia corretta?");
        dom.setOpzioni(4,"Socquadro","Soccuadro","Soquadro","Soqquadro");
        dom.setRisposteGiuste(1,"Soqquadro");
        contenitore.add(dom);
        dom = new Domanda("Storia",1,2);
        dom.setTesto("Chi erano i \"punici\" per gli antichi romani?");
        dom.setOpzioni(4,"I cartaginesi","Gli illiri","Gli etruschi","I goti");
        dom.setRisposteGiuste(1,"I cartaginesi");
        contenitore.add(dom);
        dom = new Domanda("Storia",1,3);
        dom.setTesto("Qual è la città natale di Pitagora, famoso matematico e filosofo greco?");
        dom.setOpzioni(4,"Samo","Mileto","Sibari","Crotone");
        dom.setRisposteGiuste(1,"Samo");
        contenitore.add(dom);
        dom = new Domanda("Grammatica",1,2);
        dom.setTesto("Quale tra queste è la grafia corretta?");
        dom.setOpzioni(4,"Masachusets","Massachusets","Masachusetts","Massachusetts");
        dom.setRisposteGiuste(1,"Massachusetts");
        contenitore.add(dom);
        dom = new Domanda("Grammatica",2,1);
        dom.setTesto("In quali dei seguenti casi non è corretto utilizzare l'apostrofo?");
        dom.setOpzioni(4,"D'altronde","Un'aiuto","Qual'é","C'é");
        dom.setRisposteGiuste(2,"Un'aiuto","Qual'é");
        contenitore.add(dom);
        dom = new Domanda("Geografia",1,3);
        dom.setTesto("Quale città fu soprannominata \"La Parigi degli anni Venti negli anni Novanta\" e \"La città delle cento torri\"?");
        dom.setOpzioni(4,"Bologna","Praga","Stoccolma","Ankara");
        dom.setRisposteGiuste(1,"Praga");
        contenitore.add(dom);
        dom = new Domanda("Geografia",1,3);
        dom.setTesto("Qual è la capitale dell'Irlanda del Nord?");
        dom.setOpzioni(4,"Dublino","Londra","Belfast","Cork");
        dom.setRisposteGiuste(1,"Belfast");
        contenitore.add(dom);
        dom = new Domanda("Storia",1,2);
        dom.setTesto("Quale libertà concesse l'imperatore Costantino mediante l'Editto di Milano?");
        dom.setOpzioni(4,"Di espressione","Di culto","Di stampa","Di circolazione");
        dom.setRisposteGiuste(1,"Di culto");
        contenitore.add(dom);
        dom = new Domanda("Storia",1,3);
        dom.setTesto("Qual era la moneta più antica d'Europa prima dell'introduzione dell'euro?");
        dom.setOpzioni(4,"Dracma greca","Franco francese","Sterlina inglese","Corona svedese");
        dom.setRisposteGiuste(1,"Dracma greca");
        contenitore.add(dom);
        dom = new Domanda("Arte&Letteratura",1,3);
        dom.setTesto("Quale tra queste opere è stata scritta da Giuseppe Verdi?");
        dom.setOpzioni(4,"Don Giovanni","La gazza ladra","Giovanna D'arco","Le nozze di Figaro");
        dom.setRisposteGiuste(1,"Giovanna D'arco");
        contenitore.add(dom);
        dom = new Domanda("Arte&Letteratura",1,1);
        dom.setTesto("Qual era la terza fiera che sbarrò il cammino a Dante nel primo canto della Divina Commedia, oltre al leone e alla lupa?");
        dom.setOpzioni(4,"La iena","La lonza","Il grifone","Il cinghiale");
        dom.setRisposteGiuste(1,"La lonza");
        dom.setPicture(R.drawable.dantee);
        contenitore.add(dom);
        dom = new Domanda("Arte&Letteratura",3,2);
        dom.setTesto("Qual è il nome del Brunelleschi, grande architetto e scultore rinascimentale?");
        dom.setRisposteGiuste(1,"Filippo");
        dom.setPicture(R.drawable.brunelleschi);
        contenitore.add(dom);
        dom = new Domanda("Arte&Letteratura",3,2);
        dom.setTesto("In quanti giorni Phileas Fogg avrebbe dovuto compiere il giro del mondo in un famoso romanzo di Jules Verne?");
        dom.setRisposteGiuste(1,"80");
        contenitore.add(dom);
        dom = new Domanda("Arte&Letteratura",1,1);
        dom.setTesto("Dylan Dog, uno degli eroi dei fumetti più amati di casa Bonelli, è noto anche come l'indagatore...?");
        dom.setOpzioni(4,"Del sogno","Dell'incubo","Del dormiveglia","Della pennichella");
        dom.setRisposteGiuste(1,"Dell'incubo");
        dom.setPicture(R.drawable.dylan_dog);
        contenitore.add(dom);
        dom = new Domanda("Arte&Letteratura",1,2);
        dom.setTesto("In quale città italiana possiamo ammirare i Bronzi di Riace?");
        dom.setOpzioni(4,"Reggio Calabria","Napoli","Torino","Genova");
        dom.setRisposteGiuste(1,"Reggio Calabria");
        contenitore.add(dom);
        dom = new Domanda("Scienze&affini",1,3);
        dom.setTesto("Quale organo secerne l'insulina?");
        dom.setOpzioni(4,"Il fegato","La milza","L'intestino","Il pancreas");
        dom.setRisposteGiuste(1,"Il pancreas");
        contenitore.add(dom);
        dom = new Domanda("Arte&Letteratura",1,2);
        dom.setTesto("A quale noto compositore si deve \"Il lago dei cigni\"?");
        dom.setOpzioni(4,"Liszt","Stravinsky","Tchaikovsky","Ravel");
        dom.setRisposteGiuste(1,"Tchaikovsky");
        dom.setPicture(R.drawable.lago_cigni);
        contenitore.add(dom);
        dom = new Domanda("Arte&Letteratura",1,1);
        dom.setTesto("A quale personaggio storico è dedicata l'ode \"Il cinque maggio\" di Alessandro Manzoni?");
        dom.setOpzioni(4,"Napoleone","Garibaldi","Mazzini","Cavour");
        dom.setRisposteGiuste(1,"Napoleone");
        contenitore.add(dom);
        dom = new Domanda("Arte&Letteratura",1,2);
        dom.setTesto("Da quale eroe della mitologia greca venne sconfitto il Minotauro?");
        dom.setOpzioni(4,"Teseo","Dedalo","Egeo","Minosse");
        dom.setRisposteGiuste(1,"Teseo");
        dom.setPicture(R.drawable.minotauro);
        contenitore.add(dom);
        dom = new Domanda("Grammatica",2,1);
        dom.setTesto("Quali tra questi sono sinonimi della parola \"pigro\"?");
        dom.setOpzioni(4,"Fanfarone","Svogliato","Gradasso","Indolente");
        dom.setRisposteGiuste(2,"Svogliato","Indolente");
        contenitore.add(dom);

        dom = new Domanda("Scienze&affini",1,2);
        dom.setTesto("Che cosa sono gli Omega-3?");
        dom.setOpzioni(4,"Proteine","Vitamine","Acidi grassi","Enzimi");
        dom.setRisposteGiuste(1,"Acidi grassi");
        contenitore.add(dom);
        dom = new Domanda("Scienze&affini",1,1);
        dom.setTesto("Il trattato internazionale sul riscaldamento globale dell'11 dicembre 1997 è il protocollo di:");
        dom.setOpzioni(4,"Kyoto","Tokio","Osaka","Sapporo");
        dom.setRisposteGiuste(1,"Kyoto");
        contenitore.add(dom);
        dom = new Domanda("Geografia",2,3);
        dom.setTesto("Quali di queste città si trovano nello stato del Nevada?");
        dom.setOpzioni(4,"Las Vegas","Carson City","Houston","Miami");
        dom.setRisposteGiuste(2,"Las Vegas","Carson City");
        contenitore.add(dom);
        dom = new Domanda("Scienze&affini",3,3);
        dom.setTesto("Quante vertebre ha in totale un umano adulto?");
        dom.setRisposteGiuste(1,"26");
        contenitore.add(dom);
        dom = new Domanda("Fun",1,1);
        dom.setTesto("Come si chiama il ricco industriale interpretato da Robert Downey Jr. in Iron Man?");
        dom.setOpzioni(4,"Bruce Wayne","Clark Kent","Peter Parker","Tony Stark");
        dom.setRisposteGiuste(1,"Tony Stark");
        contenitore.add(dom);
        dom = new Domanda("Fun",1,1);
        dom.setTesto("Quale di queste parole non ha nulla a che vedere con l'informatica?");
        dom.setOpzioni(4,"Desktop","Browser","Screen saver","Discount");
        dom.setRisposteGiuste(1,"Discount");
        contenitore.add(dom);
        dom = new Domanda("Scienze&affini",1,3);
        dom.setTesto("Dove si trova, nel corpo umano, il muscolo sartorio?");
        dom.setOpzioni(4,"Nella spalla","Nell'avambraccio","Nella mano","Nella coscia");
        dom.setRisposteGiuste(1,"Nella coscia");
        contenitore.add(dom);
        dom = new Domanda("Fun",1,1);
        dom.setTesto("Come si chiama nel linguaggio del web il messaggio deliberatamente provocatorio inviato da un utente di una comunità?");
        dom.setOpzioni(4,"Blaze","Blame","Flame","Frame");
        dom.setRisposteGiuste(1,"Flame");
        contenitore.add(dom);
        dom = new Domanda("Fun",1,2);
        dom.setTesto("Chi ha dato il volto a Novecento nella riduzione disneyana del libro omonimo di A.Baricco?");
        dom.setOpzioni(4,"Topolino","Paperino","Pippo","Gastone");
        dom.setRisposteGiuste(1,"Pippo");
        contenitore.add(dom);
        dom = new Domanda("Fun",1,2);
        dom.setTesto("Come si chiama l'ospedale pubblico di Treviso?");
        dom.setOpzioni(4,"Mà Leducato","Cà Foncello","Zò Ticone","Cò Atto");
        dom.setRisposteGiuste(1,"Cà Foncello");
        contenitore.add(dom);
        dom = new Domanda("Storia",1,1);
        dom.setTesto("Da chi era usata principalmente, nel Medio Evo, l'imbarcazione chiamata \"drakkar\"?");
        dom.setOpzioni(4,"Spagnoli","Greci","Francesi","Norvegesi");
        dom.setRisposteGiuste(1,"Norvegesi");
        dom.setPicture(R.drawable.drakkar);
        contenitore.add(dom);
        dom = new Domanda("Fun",1,1);
        dom.setTesto("Quale tra questi nomi non designa una categoria di videogiochi?");
        dom.setOpzioni(4,"Sparatutto","Spezzaschiena","Rompicapo","Picchiaduro");
        dom.setRisposteGiuste(1,"Spezzaschiena");
        contenitore.add(dom);

        dom = new Domanda("Geografia",2,3);
        dom.setTesto("Quali di questi Paesi dell'Unione europea non hanno ancora adottato l'euro?");
        dom.setOpzioni(4,"Romania","Lettonia","Grecia","Repubblica Ceca");
        dom.setRisposteGiuste(2,"Romania","Repubblica Ceca");
        dom.setPicture(R.drawable.euro);
        contenitore.add(dom);
        dom = new Domanda("Scienze&affini",3,2);
        dom.setTesto("Quale pianeta del Sistema Solare hanno visitato le sonde Spirit e Opportunity?");
        dom.setRisposteGiuste(1,"Marte");
        dom.setPicture(R.drawable.spirit);
        contenitore.add(dom);
        dom = new Domanda("Storia",1,2);
        dom.setTesto("Chi fondò nel 1831 il partito della \"Giovine Italia\"?");
        dom.setOpzioni(4,"Giuseppe Mazzini","Fratelli Bandiera","Silvio Pellico","Giuseppe Garibaldi");
        dom.setRisposteGiuste(1,"Giuseppe Mazzini");
        dom.setPicture(R.drawable.giovine_italia);
        contenitore.add(dom);
        dom = new Domanda("Scienze&affini",2,1);
        dom.setTesto("Quali di questi pianeti sono più piccoli della Terra?");
        dom.setOpzioni(4,"Marte","Saturno","Venere","Urano");
        dom.setRisposteGiuste(2,"Marte","Venere");
        contenitore.add(dom);
        dom = new Domanda("Scienze&affini",3,2);
        dom.setTesto("Nell'atmosfera terrestre l'ossigeno è presente in una percentuale pari a circa il:");
        dom.setRisposteGiuste(2,"20%","20 %");
        contenitore.add(dom);
        dom = new Domanda("Geografia",2,3);
        dom.setTesto("Quali di questi Paesi non si trovano nello stesso fuso orario dell'Italia?");
        dom.setOpzioni(4,"Spagna","Romania","Norvegia","Finlandia");
        dom.setRisposteGiuste(2,"Romania","Finlandia");
        contenitore.add(dom);

        dom = new Domanda("Arte&Letteratura",1,2);
        dom.setTesto("Chi è l'autore di \"Cuore di tenebra\"?");
        dom.setOpzioni(4,"J.Conrad","K.Amis","E.A.Poe","H.P.Lovecraft");
        dom.setRisposteGiuste(1,"J.Conrad");
        contenitore.add(dom);
        dom = new Domanda("Geografia",3,1);
        dom.setTesto("La catena montuosa degli Urali separa l'Europa da quale continente? (omettere l'articolo)");
        dom.setRisposteGiuste(1,"Asia");
        dom.setPicture(R.drawable.urali);
        contenitore.add(dom);
        dom = new Domanda("Geografia",3,2);
        dom.setTesto("Con quale continente collegava l'Europa la cosiddetta \"Via della seta\"? (omettere l'articolo)");
        dom.setRisposteGiuste(1,"Asia");
        contenitore.add(dom);
        dom = new Domanda("Geografia",3,1);
        dom.setTesto("In quale Stato si trova la città di Monaco di Baviera? (omettere la preposizione)");
        dom.setRisposteGiuste(1,"Germania");
        dom.setPicture(R.drawable.monaco);
        contenitore.add(dom);
        dom = new Domanda("Arte&Letteratura",1,2);
        dom.setTesto("Una tra le più famose opere di Antonio Canova è:");
        dom.setOpzioni(4,"Dedalo e Psiche","Amore e Psiche","Amore e Icaro","Psiche e Teseo");
        dom.setRisposteGiuste(1,"Amore e Psiche");
        dom.setPicture(R.drawable.amore_psiche);
        contenitore.add(dom);
        dom = new Domanda("Storia",1,1);
        dom.setTesto("Quale personaggio storico disse: \"Tu quoque, Brute, fili mi!\"?");
        dom.setOpzioni(4,"Carlo Magno","Giulio Cesare","Alessandro Magno","Ottaviano");
        dom.setRisposteGiuste(1,"Giulio Cesare");
        contenitore.add(dom);
        dom = new Domanda("Geografia",1,2);
        dom.setTesto("Dove si trova la zona del Gargano?");
        dom.setOpzioni(4,"in Piemonte","in Umbria","in Puglia","in Sardegna");
        dom.setRisposteGiuste(1,"in Puglia");
        dom.setPicture(R.drawable.gargano);
        contenitore.add(dom);
        dom = new Domanda("Scienze&affini",3,1);
        dom.setTesto("La radice cubica di 125 è...");
        dom.setRisposteGiuste(1,"5");
        contenitore.add(dom);
        dom = new Domanda("Fun",3,1);
        dom.setTesto("Negli scacchi, l'alfiere si muove solo in...");
        dom.setRisposteGiuste(2,"diagonale","Diagonale");
        dom.setPicture(R.drawable.alfiere);
        contenitore.add(dom);
        dom = new Domanda("Scienze&affini",3,2);
        dom.setTesto("Cosa apportano principalmente alla nostra alimentazione le uova di gallina?");
        dom.setRisposteGiuste(2,"proteine","Proteine");
        dom.setPicture(R.drawable.uova);
        contenitore.add(dom);
        dom = new Domanda("Geografia",1,2);
        dom.setTesto("Qual è la capitale della Norvegia?");
        dom.setOpzioni(4,"Helsinki","Reykjavik","Oslo","Copenaghen");
        dom.setRisposteGiuste(1,"Oslo");
        contenitore.add(dom);
        dom = new Domanda("Scienze&affini",3,2);
        dom.setTesto("Che percentuale della superficie terrestre ricopre l'insieme di oceani e mari?");
        dom.setRisposteGiuste(2,"70%","70 %");
        contenitore.add(dom);
        dom = new Domanda("Storia",3,3);
        dom.setTesto("Come è conosciuta in italiano la città croata di Rijeka?");
        dom.setRisposteGiuste(1,"Fiume");
        dom.setPicture(R.drawable.rjeka);
        contenitore.add(dom);
        dom = new Domanda("Arte&Letteratura",1,1);
        dom.setTesto("\"La donzelletta vien dalla campagna in sul calar del sole\": quale poeta ha scritto questi versi?");
        dom.setOpzioni(4,"G.Carducci","G.Pascoli","G.Leopardi","G.D'Annunzio");
        dom.setRisposteGiuste(1,"G.Leopardi");
        contenitore.add(dom);
        dom = new Domanda("Arte&Letteratura",1,3);
        dom.setTesto("Quale figura della mitologia greca rendeva di pietra chiunque la guardasse?");
        dom.setOpzioni(4,"Idra","Medusa","Arianna","Persefone");
        dom.setRisposteGiuste(1,"Medusa");
        contenitore.add(dom);
        dom = new Domanda("Arte&Letteratura",2,2);
        dom.setTesto("Quali fra questi titoli non appartengono a un'opera di Luigi Pirandello?");
        dom.setOpzioni(4,"Il barone rampante","La giara","I Malavoglia","La patente");
        dom.setRisposteGiuste(2,"Il barone rampante","I Malavoglia");
        contenitore.add(dom);
        dom = new Domanda("Storia",3,2);
        dom.setTesto("Nel 79 d.C. la città di Pompei fu distrutta dall'eruzione del vulcano...");
        dom.setRisposteGiuste(1,"Vesuvio");
        dom.setPicture(R.drawable.pompei);
        contenitore.add(dom);
        dom = new Domanda("Storia",3,3);
        dom.setTesto("Quale fu la prima capitale del Regno d'Italia?");
        dom.setRisposteGiuste(1,"Torino");
        contenitore.add(dom);
        dom = new Domanda("Storia",1,3);
        dom.setTesto("Qual è il nome dell'influenza che causò la morte di 50 milioni di persone in tutto il mondo tra il 1918 e il 1919?");
        dom.setOpzioni(4,"Influenza francese","Influenza greca","Influenza bulgara","Influenza spagnola");
        dom.setRisposteGiuste(1,"Influenza spagnola");
        contenitore.add(dom);


        //dbHelper.deleteAllTuples();
        dbHelper.addContenitore(contenitore);
        hideProgressDialog();
        Log.i(TAG,"Aggiunto le domande al DB");
    }



    }





