package a20014022.thequiz;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Ale on 25/01/2018.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String TAG = "LOD";
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "myquiz.db";
    // names of table and columns
    public static final String TABLE_NAME = "questions";
    public static final String CATEGORIA = "cat";
    public static final String LIVELLO = "lv";
    public static final String TIPO = "tipo";
    public static final String TESTO = "testo";
    public static final String IMMAGINE = "pic";
    public static final String OPZIONEA = "OA";
    public static final String OPZIONEB = "OB";
    public static final String OPZIONEC = "OC";
    public static final String OPZIONED = "OD";
    public static final String RISPOSTAA = "RA";
    public static final String RISPOSTAB = "RB";

    // create and drop commands
    public static final String SQL_CREATE = "create table " + TABLE_NAME +
            " (_id integer primary key autoincrement, " + CATEGORIA + " varchar(30) not null, "
            + LIVELLO + " integer not null, " + TIPO + " integer not null, " + TESTO + " text not null, "
            + IMMAGINE + " integer, " + OPZIONEA + " varchar(30), " + OPZIONEB + " varchar(30), "
            + OPZIONEC + " varchar(30), " + OPZIONED + " varchar(30), "
            + RISPOSTAA + " varchar(30) not null, " + RISPOSTAB + " varchar(30))";
    public static final String SQL_DROP = "DROP TABLE IF EXISTS "+TABLE_NAME;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        Log.w(TAG,"onCreate DB");
        db.execSQL(SQL_CREATE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // upgrade policy is to simply to discard the data and start over
        db.execSQL(SQL_DROP);
        onCreate(db);
    }

    public void deleteAllTuples(){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("delete from "+ TABLE_NAME);
        Log.i(TAG, "Ho eliminato tutte le righe dalla tabella");
        db.close();
    }

    void addContenitore(ArrayList<Domanda> contenitore){
        //int random = 1;
        String [] columns = {"_id",CATEGORIA,LIVELLO,TIPO,TESTO,OPZIONEA,OPZIONEB,OPZIONEC,OPZIONED,RISPOSTAA,RISPOSTAB};
        //Domanda dom = new Domanda("Geografia",1,2);
        //dom.setTesto("Qual è lo Stato con la popolazione ispanofona più numerosa?");
        try {
            SQLiteDatabase db = getReadableDatabase();
            Cursor c = db.query(TABLE_NAME, columns, null, null, null, null, null);
            int count = c.getCount();
            if(count==0){
                Log.i(TAG, "Non Ci sono già delle tuple nella tabella");
                //c = db.query(TABLE_NAME, columns, null, null, null, null, null);
                //Log.i(TAG, "id ritornato: " + c.toString()+"c.getCount: "+c.getCount());
                for(Domanda temp : contenitore) {
                    if(checkifNotinDatabase(temp)) {
                        Log.i(TAG, "Sto passando all'addQuestion la domanda: " + temp.getTesto());
                        addQuestion(temp);
                    }
                }
            }
            c.close(); db.close();
        } catch (Throwable t) {Log.e(TAG,"getRows: " + t.toString(),t);}
    }



    // add a new question to the db
    void addQuestion(Domanda domanda) {
        ContentValues values = new ContentValues();
        values.put(CATEGORIA, domanda.getCategoria());
        values.put(LIVELLO, domanda.getLivello());
        values.put(TIPO, domanda.getTipo());
        Log.i(TAG,"addQuestion. Tipo = " + domanda.getTipo());
        values.put(TESTO, domanda.getTesto());
        if(domanda.getTipo()!=3){
            ArrayList<String> opzioni = domanda.getOpzioni();
            values.put(OPZIONEA, opzioni.get(0));
            values.put(OPZIONEB, opzioni.get(1));
            values.put(OPZIONEC, opzioni.get(2));
            values.put(OPZIONED, opzioni.get(3));
        }
        ArrayList<String> risposte = domanda.getRisposteGiuste();
        Log.i(TAG,"addQuestion. risposte.size:" + risposte.size());
        values.put(RISPOSTAA, risposte.get(0));
        if(risposte.size()==2) values.put(RISPOSTAB, risposte.get(1));

        if(domanda.getPicture()!=0) {
            Log.i(TAG,"Immagine:" + domanda.getPicture());
            values.put(IMMAGINE,domanda.getPicture());
        }
        else values.put(IMMAGINE,0);
        SQLiteDatabase db = getReadableDatabase();
        // Insert the new row, returning the primary key value of the new row
        long id = db.insert(DatabaseHelper.TABLE_NAME, null, values);
        db.close();
        Log.i(TAG,"Database insertion returned:" + id);
    }

    private Domanda recuperaDomanda(Cursor c){
        String categoria = c.getString(c.getColumnIndexOrThrow(CATEGORIA));
        int livello = c.getInt(c.getColumnIndexOrThrow(LIVELLO));
        int tipo = c.getInt(c.getColumnIndexOrThrow(TIPO));
        Domanda dom = new Domanda(categoria,tipo,livello);
        dom.setTesto(c.getString(c.getColumnIndexOrThrow(TESTO)));
        Log.i(TAG, "" + dom.getTesto());
        if(tipo!=3){
            dom.setOpzioni(4,c.getString(c.getColumnIndexOrThrow(OPZIONEA)),
                    c.getString(c.getColumnIndexOrThrow(OPZIONEB)),
                    c.getString(c.getColumnIndexOrThrow(OPZIONEC)),
                    c.getString(c.getColumnIndexOrThrow(OPZIONED)));
        }
        String rispostaA = c.getString(c.getColumnIndexOrThrow(RISPOSTAA));
        String rispostaB = c.getString(c.getColumnIndexOrThrow(RISPOSTAB));
        if(rispostaB!=null) dom.setRisposteGiuste(2,rispostaA,rispostaB);
        else dom.setRisposteGiuste(1,rispostaA);
        int imm = c.getInt(c.getColumnIndexOrThrow(IMMAGINE));
        if(imm!=0) dom.setPicture(imm);
        return dom;
    }


    boolean checkifNotinDatabase(Domanda domanda){
        String [] columns = {"_id",CATEGORIA,LIVELLO,TIPO,TESTO,OPZIONEA,OPZIONEB,OPZIONEC,OPZIONED,RISPOSTAA,RISPOSTAB,IMMAGINE};
        Domanda dom = null;
        Log.i(TAG, "" + domanda.toString());
        try {
            SQLiteDatabase db = getReadableDatabase();
            Cursor c = db.query(TABLE_NAME, columns, null, null, null, null, null);
            Log.i(TAG,""+c.getCount());
            while (c.moveToNext()) {
                dom = recuperaDomanda(c);

                Log.i(TAG,"domanda nel database: "+dom.toString());
                Log.i(TAG,"domanda da inserire: "+domanda.toString());
                if(dom.equals(domanda)){
                    //la domanda esiste già nel database. Non sarà da aggiungere
                    return false;
                }
            }
            c.close(); db.close();
        } catch (Throwable t) {Log.e(TAG,"getRows: " + t.toString(),t);}
        //Non ho trovato corrispondenze. La domanda in input non esiste nel database
        return true;
    }

        DomandaDB getRandomQuestion(ArrayList<Integer> posizioniSoluzioni){
        //int random = 1;
        String [] columns = {"_id",CATEGORIA,LIVELLO,TIPO,TESTO,OPZIONEA,OPZIONEB,OPZIONEC,OPZIONED,RISPOSTAA,RISPOSTAB};
        String whereClause = " WHERE _id = ?";
        String[] whereArgs;
        ArrayList<Integer> posizioni;
        Domanda dom = null;
        DomandaDB domDB = new DomandaDB();
        try {
            SQLiteDatabase db = getReadableDatabase();
            Cursor c = db.query(TABLE_NAME, columns, null, null, null, null, null);
            int count = c.getCount();
            if(count==0){
                Log.i(TAG, "Non ci sono tuple nella tabella");
                return null;
            }
            if(count == posizioniSoluzioni.size()){
                //caso in cui ho già mostrato all'utente tutte le domande di cui disponevo
                Log.i(TAG,"Non ci sono più domande. Ritorno null alla MainActivity");
                return null;
            }
            //Queste righe mi servono perchè gli id delle domande all'interno del database potrebbero essere alti
            //(in base a quante deleteAllTuples si sono fatte)
            posizioni = new ArrayList<>();
            while (c.moveToNext()) {
                int _id = c.getInt(c.getColumnIndexOrThrow("_id"));
                //Log.i(TAG, "id: "+_id);
                posizioni.add(_id);
            }
            c.close();
            int random;
            int pos;
            boolean flag;
            //Ciclo per scegliere delle domande non scelte nello stesso quiz
            do{
                flag = false;
                random = (int) (Math.random() * count);
                Log.i(TAG, "Random: " + random);
                pos = posizioni.get(random);
                for(Integer temp : posizioniSoluzioni){
                    if(pos==temp) flag = true;
                }
            }while(flag);
            whereArgs = new String[]{""+pos};
            c = db.rawQuery("SELECT * FROM "+TABLE_NAME+ whereClause,whereArgs);
            Log.i(TAG, "id ritornato: " + c.toString()+"c.getCount: "+c.getCount());
            domDB.setId(posizioni.get(random));
            while (c.moveToNext()) {
                dom = recuperaDomanda(c);
            }
            c.close(); db.close();
        } catch (Throwable t) {Log.e(TAG,"getRows: " + t.toString(),t);}
        Log.i(TAG,""+dom.toString());
        domDB.setDomanda(dom);
        return domDB;
    }

    DomandaDB getSpecificQuestion(ArrayList<Integer> posizioniSoluzioni,String whereClause,String[] whereArgs){
        //int random = 1;
        //String [] columns = {"_id",CATEGORIA,LIVELLO,TIPO,TESTO,OPZIONEA,OPZIONEB,OPZIONEC,OPZIONED,RISPOSTAA,RISPOSTAB};
        Domanda dom = null;
        DomandaDB domDB = new DomandaDB();
        ArrayList<Integer> posizioni;
        try {
            SQLiteDatabase db = getReadableDatabase();
            Log.i(TAG,"whereClause: "+whereClause.toString()+"; whereArgs: "+whereArgs[0]);
            Cursor c = db.rawQuery("SELECT * FROM "+TABLE_NAME+ whereClause,whereArgs);
            Log.i(TAG, "id ritornato: " + c.toString()+"c.getCount: "+c.getCount());
            int count = c.getCount();
            if(count==0){
                Log.i(TAG, "Non ci sono tuple nella tabella");
                return null;
            }
            if(count == posizioniSoluzioni.size()){
                //caso in cui ho già mostrato all'utente tutte le domande di cui disponevo di quella certa categoria/tipo
                //quindi non ce ne sono altre
                Log.i(TAG,"Non ci sono più domande di questa categoria. Ritorno null alla MainActivity");
                return null;
            }
            posizioni = new ArrayList<>();
            while (c.moveToNext()) {
                int _id = c.getInt(c.getColumnIndexOrThrow("_id"));
                Log.i(TAG, "id: "+_id);
                posizioni.add(_id);
            }
            int random;
            int pos;
            boolean flag;
            //Ciclo per scegliere delle domande non scelte nello stesso quiz
            do{
                flag = false;
                random = (int) (Math.random() * count);
                Log.i(TAG, "Random: " + random);
                pos = posizioni.get(random);
                for(Integer temp : posizioniSoluzioni){
                    if(pos==temp) flag = true;
                }
            }while(flag);
            whereClause = " WHERE _id = ? ";
            String[] whereArgs2 = new String[1];
            whereArgs2[0] = ""+pos;
            Log.i(TAG, "whereClause: " + whereClause+"whereArgs: "+whereArgs2[0]);
            c = db.rawQuery("SELECT * FROM "+TABLE_NAME+ whereClause,whereArgs2);
            domDB.setId(pos);
            while (c.moveToNext()) {
                dom = recuperaDomanda(c);
            }
            c.close(); db.close();
        } catch (Throwable t) {Log.e(TAG,"getRows: " + t.toString(),t);}
        Log.i(TAG,""+dom.toString());
        domDB.setDomanda(dom);
        return domDB;
    }

    ArrayList<String> dammiLeCategorie(){
        String [] columns = {CATEGORIA};
        ArrayList<String> listaCategorie = new ArrayList<>();
        try {
            SQLiteDatabase db = getReadableDatabase();
            Cursor c = db.query(TABLE_NAME, columns, null, null, null, null, null);
            boolean flag;
            while (c.moveToNext()) {
                flag = false;
                String categoria = c.getString(c.getColumnIndexOrThrow(CATEGORIA));
                //Log.i(TAG, "" + categoria);
                for(String temp : listaCategorie){
                    if(temp.equals(categoria)) flag = true;
                }
                if(!flag) listaCategorie.add(categoria);
            }
            c.close(); db.close();
        } catch (Throwable t) {Log.e(TAG,"getRows: " + t.toString(),t);}
        Log.i(TAG, "" + listaCategorie.toString());
        return listaCategorie;
    }


}
