package a20014022.thequiz;

import java.util.ArrayList;

/**
 * Created by Ale on 07/02/2018.
 */

public class Quiz {

    ArrayList<Domanda> quiz;

    public Quiz(){}

    public ArrayList<Domanda> getQuiz() {
        return quiz;
    }

    public void setQuiz(ArrayList<Domanda> quiz) {
        this.quiz = quiz;
    }

    @Override
    public String toString() {
        return "Quiz{" +
                "quiz=" + quiz +
                '}';
    }
}
