package a20014022.thequiz;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Ale on 03/02/2018.
 */

public class User implements Parcelable {
    private String nick;
    private String email;
    private float punteggio;
    private ArrayList<String> chiaviQuizFatti;

    public User() {}
    public User(String nick, String email, float punteggio) {
        this.nick = nick;
        this.email = email;
        this.punteggio = punteggio;
        chiaviQuizFatti = new ArrayList<>();
    }

    public String getNick() {
        return nick;
    }

    public String getEmail() {
        return email;
    }

    public float getPunteggio() {
        return punteggio;
    }

    public ArrayList<String> getChiaviQuizFatti() {
        return chiaviQuizFatti;
    }

    public void addKeytoArrayList(String key){
        if(chiaviQuizFatti==null) chiaviQuizFatti = new ArrayList<>();
        chiaviQuizFatti.add(key);
    }

    @Override
    public String toString() {
        return "User{" +
                "nick='" + nick + '\'' +
                ", email='" + email + '\'' +
                ", punteggio=" + punteggio +
                ", chiaviQuizFatti=" + chiaviQuizFatti +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Actual object serialization happens here, Write object content
     * to parcel, reading should be done according to this write order
     * param dest - parcel
     * param flags - Additional flags about how the object should be written
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.nick);
        dest.writeString(this.email);
        dest.writeFloat(this.punteggio);
        dest.writeList(this.chiaviQuizFatti);
    }

    /**
     * Use when reconstructing User object from parcel
     * This will be used only by the 'CREATOR'
     * @param in a parcel to read this object
     */
    public User(Parcel in) {
        this.nick = in.readString();
        this.email = in.readString();
        this.punteggio = in.readFloat();
        this.chiaviQuizFatti = in.readArrayList(ArrayList.class.getClassLoader());
    }


    /**
     * This field is needed for Android to be able to
     * create new objects, individually or as arrays
     *
     * If you don’t do that, Android framework will through exception
     * Parcelable protocol requires a Parcelable.Creator object called CREATOR
     */
    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {

        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };


}