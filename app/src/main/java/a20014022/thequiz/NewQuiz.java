package a20014022.thequiz;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

/**
 * Created by Ale on 26/01/2018.
 */

public class NewQuiz extends AppCompatActivity {

    //DatabaseHelper dbHelper;
    public static String TAG = "QUIZZ";
    Button rispA;
    Button rispB;
    Button rispC;
    Button rispD;
    Button confirm;
    Button dont_know;
    Domanda domanda;
    int bottoneScelto;
    boolean flag = false;
    ImageView imv;
    int countSelezionati = 0;
    float punteggio = -1;
    boolean ultimaDomanda;
    int secondiRimanenti;
    TextView limiteTempo;
    float puntiRimanenti;
    ImageButton btnMicrophone;
    private final int SPEECH_RECOGNITION_CODE = 1;
    private StorageReference mStorageRef;
    String domandaStorage;
    ValueEventListener questionsListener;
    private DatabaseReference myRefQuestion; // reference to new questions
    private FirebaseDatabase database;// remote DB
    int quanteDomande;
    int quante;
    ImageView livello;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
        //dbHelper = new DatabaseHelper(getApplicationContext());
        //domanda = dbHelper.getRandomQuestion();
        Log.i(TAG, "Sto per recuperare la domanda");
        domanda = getIntent().getParcelableExtra("NuovaDomanda");
        ultimaDomanda = getIntent().getBooleanExtra("Ultima",false);
        secondiRimanenti = getIntent().getIntExtra("Secondi",-1);
        puntiRimanenti = getIntent().getFloatExtra("Punti",-3);
        Log.i(TAG, "Presa la domanda casuale: "+domanda.toString());
        quanteDomande = getIntent().getIntExtra("quanteDomande",0);
        quante = getIntent().getIntExtra("quante",0);
        database = FirebaseDatabase.getInstance();
        myRefQuestion = database.getReference("newQuestions");

        questionsListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                Domanda nuova_domanda;
                for(DataSnapshot child : dataSnapshot.getChildren()){
                    nuova_domanda = child.getValue(Domanda.class);
                    Log.i(TAG,"domanda su firebase:"+nuova_domanda.toString()+"\ndomanda nel database interno: "+domanda.toString());
                    if(nuova_domanda.equals(domanda)){
                        Log.i(TAG,"Ok, l'ho trovata! child.getKey: "+child.getKey());
                        domandaStorage = child.getKey();
                        checkIfFotoStorage();
                        break;
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.i(TAG, "Failed to read value.", error.toException());
            }
        };

        if(domanda.getTipo()==1){
            //CASO DOMANDA SINGLE CHOICE
            settoBottoni();
            rispA.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OnClickButtonforSC(v);
                }
            });
            rispB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OnClickButtonforSC(v);
                }
            });
            rispC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OnClickButtonforSC(v);
                }
            });
            rispD.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OnClickButtonforSC(v);
                }
            });
            confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickConfirmButtonforSC(v);
                }
            });
            Log.i(TAG, "Settato le opzioni");
        }
        else if(domanda.getTipo()==3){
            //CASO DOMANDA OPEN
            setContentView(R.layout.domanda_aperta);
            TextView tv = (TextView)findViewById(R.id.open_question);
            tv.setText(domanda.getTesto());
            Log.i(TAG, "Ho preso una domanda a risposta aperta; ");
            settaImmagine();
            settaIconaDifficolta();
            btnMicrophone = (ImageButton) findViewById(R.id.btn_mic);
            btnMicrophone.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    startSpeechToText();
                }
            });
        }
        else{
            //CASO DOMANDA MULTIPLE CHOICE
            settoBottoni();
            rispA.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OnClickButtonforMC(v);
                }
            });
            rispB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OnClickButtonforMC(v);
                }
            });
            rispC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OnClickButtonforMC(v);
                }
            });
            rispD.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OnClickButtonforMC(v);
                }
            });
            confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickConfirmButtonforMC(v);
                }
            });

        }

        if(puntiRimanenti!=-3){
            //score limit
            limiteTempo = (TextView)findViewById(R.id.tempo);
            limiteTempo.setVisibility(View.VISIBLE);
            limiteTempo.setText("Ti mancano:\n"+puntiRimanenti+" punti");
        }

    }


    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i(TAG,"onSaveInstanceState");
        outState.putParcelable("domanda",domanda);
        if(domanda.getTipo()!=3){
            rispA = (Button)findViewById(R.id.rispA);
            rispB = (Button)findViewById(R.id.rispB);
            rispC = (Button)findViewById(R.id.rispC);
            rispD = (Button)findViewById(R.id.rispD);
            int color = ((ColorDrawable)rispA.getBackground()).getColor();
            outState.putInt("rispA",color);
            color = ((ColorDrawable)rispB.getBackground()).getColor();
            outState.putInt("rispB",color);
            color = ((ColorDrawable)rispC.getBackground()).getColor();
            outState.putInt("rispC",color);
            color = ((ColorDrawable)rispD.getBackground()).getColor();
            outState.putInt("rispD",color);
            if(rispA.isClickable()) outState.putBoolean("opzioni_clickable",true);
            else outState.putBoolean("opzioni_clickable",false);
        }

        if(secondiRimanenti!=-1){
            recuperoSecondiRimanenti();
            Log.i(TAG,"Secondi rimanenti aggiornati: "+secondiRimanenti);
            outState.putInt("secondiRimanenti",secondiRimanenti);
        }

        outState.putInt("bottoneScelto",bottoneScelto);
        outState.putBoolean("flag",flag);
        outState.putFloat("punteggio",punteggio);
        outState.putInt("countSelezionati",countSelezionati);

        Button but = (Button)findViewById(R.id.next_question);
        if(but.getVisibility()==View.VISIBLE) outState.putBoolean("next_visibility",true);
        else outState.putBoolean("next_visibility",false);
        confirm = (Button)findViewById(R.id.confirm);
        if(confirm.isClickable()) outState.putBoolean("confirm_clickable",true);
        else outState.putBoolean("confirm_clickable",false);
        if(domanda.getTipo()==3){
            dont_know = (Button)findViewById(R.id.dont_know);
            if(dont_know.isClickable()) outState.putBoolean("dont_know_clickable",true);
            else outState.putBoolean("dont_know_clickable",false);
            btnMicrophone = (ImageButton)findViewById(R.id.btn_mic);
            if(btnMicrophone.isClickable()) outState.putBoolean("microphone_clickable",true);
            else outState.putBoolean("microphone_clickable",false);
            EditText edita = (EditText)findViewById(R.id.your_response);
            if(edita.isEnabled()) outState.putBoolean("edita_enabled",true);
            else outState.putBoolean("edita_enabled",false);
            int color = edita.getCurrentTextColor();
            if(color == getResources().getColor(android.R.color.holo_green_dark)) outState.putBoolean("giusta",true);
            else outState.putBoolean("giusta",false);
        }
    }

    protected void onRestoreInstanceState(Bundle savedState) {
        super.onRestoreInstanceState(savedState);
        Log.i(TAG,"onRestoreInstanceState");
        domanda = savedState.getParcelable("domanda");
        if(domanda.getTipo()!=3){
            rispA = (Button)findViewById(R.id.rispA);
            rispB = (Button)findViewById(R.id.rispB);
            rispC = (Button)findViewById(R.id.rispC);
            rispD = (Button)findViewById(R.id.rispD);
            int color = savedState.getInt("rispA");
            rispA.setBackgroundColor(color);
            color = savedState.getInt("rispB");
            rispB.setBackgroundColor(color);
            color = savedState.getInt("rispC");
            rispC.setBackgroundColor(color);
            color = savedState.getInt("rispD");
            rispD.setBackgroundColor(color);
            if(!savedState.getBoolean("opzioni_clickable")){
                rispA.setClickable(false);
                rispB.setClickable(false);
                rispC.setClickable(false);
                rispD.setClickable(false);
            }
        }
        if(secondiRimanenti!=-1) {
            secondiRimanenti = savedState.getInt("secondiRimanenti");
            Log.i(TAG,"Secondi rimanenti aggiornati: "+secondiRimanenti);
            //limiteTempo.setText("Ti rimangono: "+secondiRimanenti+" secondi");
        }
        bottoneScelto = savedState.getInt("bottoneScelto");
        flag = savedState.getBoolean("flag");
        punteggio = savedState.getFloat("punteggio");
        countSelezionati = savedState.getInt("countSelezionati");

        Button but = (Button)findViewById(R.id.next_question);
        if(savedState.getBoolean("next_visibility")) but.setVisibility(View.VISIBLE);
        if(ultimaDomanda) but.setText(R.string.last_question);
        confirm = (Button)findViewById(R.id.confirm);
        if(!savedState.getBoolean("confirm_clickable")) confirm.setClickable(false);
        if(domanda.getTipo()==3){
            dont_know = (Button)findViewById(R.id.dont_know);
            if(!savedState.getBoolean("dont_know_clickable")) dont_know.setClickable(false);
            btnMicrophone = (ImageButton)findViewById(R.id.btn_mic);
            if(!savedState.getBoolean("microphone_clickable")) btnMicrophone.setClickable(false);
            EditText edita = (EditText)findViewById(R.id.your_response);
            if(!savedState.getBoolean("edita_enabled")) edita.setEnabled(false);
            if(savedState.getBoolean("giusta")) edita.setTextColor(getResources().getColor(android.R.color.holo_green_dark));
            else edita.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
        }
    }

    private void recuperoSecondiRimanenti(){
        String ausiliaria = new String();
        limiteTempo = (TextView)findViewById(R.id.tempo);
        String limite = limiteTempo.getText().toString();
        for (int i = 0; i < limite.length(); i++) {
            if (Character.isDigit(limite.charAt(i))) {
                ausiliaria = ausiliaria + limite.charAt(i);
            }
        }
        secondiRimanenti = Integer.parseInt(ausiliaria);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG,"onResume");
        myRefQuestion.addListenerForSingleValueEvent(questionsListener);
        if(secondiRimanenti!=-1){
            //time limit
            limiteTempo = (TextView)findViewById(R.id.tempo);
            limiteTempo.setVisibility(View.VISIBLE);
            new CountDownTimer(secondiRimanenti*1000,1000){
                public void onTick(long millisUntilFinished){
                    limiteTempo.setText("Ti rimangono: "+millisUntilFinished/1000+" secondi");
                }
                public void onFinish(){
                    cancel();
                    limiteTempo.setText(getText(R.string.time_finished));
                    secondiRimanenti = -2;
                    Log.i(TAG,"Il tempo è scaduto. Peccato, non sei riuscito a rispondere a questa domanda... ");
                    Button v = (Button)findViewById(R.id.confirm);
                    v.setClickable(false);
                    if(domanda.getTipo()!=3){
                        rispA.setClickable(false);
                        rispB.setClickable(false);
                        rispC.setClickable(false);
                        rispD.setClickable(false);
                    }
                    if(domanda.getTipo()==3){
                        v = (Button)findViewById(R.id.dont_know);
                        v.setClickable(false);
                        btnMicrophone.setClickable(false);
                    }
                    Button but = (Button)findViewById(R.id.next_question);
                    but.setText(getText(R.string.last_question));
                    but.setVisibility(View.VISIBLE);
                }
            }.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG,"onPause");
        myRefQuestion.removeEventListener(questionsListener);
        if(secondiRimanenti!=-1 && secondiRimanenti!=-2) recuperoSecondiRimanenti();
    }



    @Override
    public void onDestroy(){
        super.onDestroy();
        //Log.i(TAG,"onDestroy");
    }

    private ArrayList<Integer> generateOptionsInRandomPosition(){
        Random random = new Random();
        ArrayList<Integer> doveLiMetto = new ArrayList<>();
        int randNum = random.nextInt(4);
        doveLiMetto.add(randNum);
        while(doveLiMetto.size()<4){
            randNum = random.nextInt(4);
            while(doveLiMetto.contains(randNum)){
                randNum = random.nextInt(4);
            }
            doveLiMetto.add(randNum);
        }
        Log.i(TAG,"doveLiMetto. 0 pos: "+doveLiMetto.get(0)+"; 1 pos: "+doveLiMetto.get(1)+"; 2 pos: "+doveLiMetto.get(2)+"; 3 pos: "+doveLiMetto.get(3));
        return doveLiMetto;
    }

    private void settoBottoni(){
        setContentView(R.layout.domanda);
        Log.i(TAG, "Caricato il layout della domanda");
        TextView tv = (TextView)findViewById(R.id.question);
        tv.setText(domanda.getTesto());
        Log.i(TAG, "Settato il testo della domanda");
        ArrayList<Integer> doveLiMetto = generateOptionsInRandomPosition();
        rispA = (Button)findViewById(R.id.rispA);
        //rispA.setText(domanda.getOpzioni().get(0));
        rispA.setText(domanda.getOpzioni().get(doveLiMetto.get(0)));
        Log.i(TAG, "Settato la prima opzione della domanda");
        rispB = (Button)findViewById(R.id.rispB);
        //rispB.setText(domanda.getOpzioni().get(1));
        rispB.setText(domanda.getOpzioni().get(doveLiMetto.get(1)));
        rispC = (Button)findViewById(R.id.rispC);
        //rispC.setText(domanda.getOpzioni().get(2));
        rispC.setText(domanda.getOpzioni().get(doveLiMetto.get(2)));
        rispD = (Button)findViewById(R.id.rispD);
        //rispD.setText(domanda.getOpzioni().get(3));
        rispD.setText(domanda.getOpzioni().get(doveLiMetto.get(3)));
        settaImmagine();
        confirm = (Button)findViewById(R.id.confirm);
        settaIconaDifficolta();
    }

    private void settaImmagine(){
        if(domanda.getPicture()!=0 && domanda.getPicture()!=-1){
            imv=(ImageView)findViewById(R.id.icon);
            imv.setImageResource(domanda.getPicture());
        }
    }

    private void settaIconaDifficolta(){
        livello = (ImageView)findViewById(R.id.diff);
        if(domanda.getLivello()==1){
            livello.setImageResource(R.drawable.facile);
        }
        else if(domanda.getLivello()==2){
            livello.setImageResource(R.drawable.media);
        }
        else livello.setImageResource(R.drawable.difficile);
    }

    private void checkIfFotoStorage(){
        imv=(ImageView)findViewById(R.id.icon);
        FirebaseStorage storage = FirebaseStorage.getInstance();
        mStorageRef = storage.getReference("FotoDomande/"+domandaStorage+".jpg");
        //mStorageRef = storage.getReferenceFromUrl("https://firebasestorage.googleapis.com/v0/b/thequiz-6eb78.appspot.com/o/FotoDomande%2F-L4qsT2gNnK5sAPp6HP9.jpg?alt=media&token=896e8c1c-c845-4f00-b3d6-deb06e768a18");
        Log.i(TAG,"cerco qui: "+"FotoDomande/"+domandaStorage+".jpg");
        try {
            Glide.with(this)
                    .using(new FirebaseImageLoader())
                    .load(mStorageRef)
                    .into(imv);
        }catch(Exception e){
            Log.i(TAG,""+e.getMessage());
        }
    }

    private void coloraBottoni(Button but1, Button but2, Button but3){
        but1.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
        but2.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
        but3.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
    }

    public void OnClickButtonforSC(View v) {
        v.setBackgroundColor(getResources().getColor(android.R.color.holo_green_light));
        bottoneScelto = v.getId();
        flag = true;
        if(v.getId()==R.id.rispA){
            coloraBottoni(rispB,rispC,rispD);
        }
        else if(v.getId()==R.id.rispB){
            coloraBottoni(rispA,rispC,rispD);
        }
        else if(v.getId()==R.id.rispC){
            coloraBottoni(rispA,rispB,rispD);
        }
        else if(v.getId()==R.id.rispD){
            coloraBottoni(rispA,rispB,rispC);
        }
        //Log.i(TAG, "Id bottone: "+v.getId()+"id RispA: "+ R.id.rispA);
    }

    public void onClickConfirmButtonforSC(View v) {
        if (!flag) {
            Toast.makeText(this, "Seleziona prima una risposta...", Toast.LENGTH_SHORT).show();
        } else {
            Button but = (Button) findViewById(bottoneScelto);
            Log.i(TAG, "Testo del bottone: " + but.getText().toString());
            Log.i(TAG, "Risposta giusta: " + domanda.getRisposteGiuste().get(0));
            //Una domanda single choice può avere una sola risposta
            if (but.getText().toString().equals(domanda.getRisposteGiuste().get(0))) {
                but.setBackgroundColor(getResources().getColor(android.R.color.holo_green_dark));
                Toast.makeText(this, "Risposta esatta!", Toast.LENGTH_SHORT).show();
                punteggio = domanda.getLivello();
                puntiRimanenti -= punteggio;
            } else {
                but.setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                Toast.makeText(this, "Risposta sbagliata!", Toast.LENGTH_SHORT).show();
                if (rispA.getText().toString().equals(domanda.getRisposteGiuste().get(0)))
                    rispA.setBackgroundColor(getResources().getColor(android.R.color.holo_green_dark));
                else if (rispB.getText().toString().equals(domanda.getRisposteGiuste().get(0)))
                    rispB.setBackgroundColor(getResources().getColor(android.R.color.holo_green_dark));
                else if (rispC.getText().toString().equals(domanda.getRisposteGiuste().get(0)))
                    rispC.setBackgroundColor(getResources().getColor(android.R.color.holo_green_dark));
                else
                    rispD.setBackgroundColor(getResources().getColor(android.R.color.holo_green_dark));
                punteggio = 0;
            }
            v.setClickable(false);
            rispA.setClickable(false);
            rispB.setClickable(false);
            rispC.setClickable(false);
            rispD.setClickable(false);
            but = (Button)findViewById(R.id.next_question);
            if(ultimaDomanda) but.setText(getText(R.string.last_question));
            but.setVisibility(View.VISIBLE);
            if(puntiRimanenti>=-2 && puntiRimanenti<=0){
                but.setText(getText(R.string.last_question));
            }
        }
    }

    public void OnClickButtonforMC(View v) {
        int color = ((ColorDrawable)v.getBackground()).getColor();
        //Log.i(TAG, "Colore trovato: "+color+"; Colore cercato: "+ getResources().getColor(android.R.color.darker_gray));
        if(color==getResources().getColor(android.R.color.darker_gray) && countSelezionati<2){
            v.setBackgroundColor(getResources().getColor(android.R.color.holo_green_light));
            countSelezionati++;
        }
        else if(color==getResources().getColor(android.R.color.holo_green_light)){
            v.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
            countSelezionati--;
        }
    }

    private int checkSingleButtonforMC(Button but){
        for(String temp : domanda.getRisposteGiuste()) {
            if (but.getText().toString().equals(temp)) {
                but.setBackgroundColor(getResources().getColor(android.R.color.holo_green_dark));
                return 0;
            }
        }
        //Ora guardo che colore ha questa opzione. Se non è verde scuro, vuol dire che non è giusta.
        //Se il suo colore è grigio, vuol dire che l'utente non l'ha selezionata
        //Altrimenti il colore è verde chiaro e quindi l'utente ha sbagliato
        int color = ((ColorDrawable)but.getBackground()).getColor();
        if(color==getResources().getColor(android.R.color.holo_green_light)){
            but.setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
            return 1;
        }
        return 0;
    }

    public void onClickConfirmButtonforMC(View v){
        if (countSelezionati<2) {
            Toast.makeText(this, "Devi selezionare due opzioni...", Toast.LENGTH_SHORT).show();
        }
        else{
            int sbagliate = 0;
            sbagliate += checkSingleButtonforMC(rispA);
            sbagliate += checkSingleButtonforMC(rispB);
            sbagliate += checkSingleButtonforMC(rispC);
            sbagliate += checkSingleButtonforMC(rispD);
            if(sbagliate==0) {
                Toast.makeText(this, "Risposta esatta!", Toast.LENGTH_SHORT).show();
                punteggio = domanda.getLivello();
            }
            else if (sbagliate==1) {
                Toast.makeText(this, "Hai sbagliato un'opzione.", Toast.LENGTH_SHORT).show();
                punteggio = ((float)domanda.getLivello())/2;
            }
            else {
                Toast.makeText(this, "Hai ciccato completamente la domanda.", Toast.LENGTH_SHORT).show();
                punteggio = 0;
            }
            puntiRimanenti -= punteggio;
            v.setClickable(false);
            rispA.setClickable(false);
            rispB.setClickable(false);
            rispC.setClickable(false);
            rispD.setClickable(false);
            Button but = (Button)findViewById(R.id.next_question);
            if(ultimaDomanda) but.setText(getText(R.string.last_question));
            but.setVisibility(View.VISIBLE);
            if(puntiRimanenti>=-2 && puntiRimanenti<=0){
                but.setText(getText(R.string.last_question));
            }
        }
    }

    public void onClickConfirmButtonforOpen(View v){
        EditText edita = (EditText)findViewById(R.id.your_response);
        if(edita.getText().toString().isEmpty()){
            Toast.makeText(this, "Non hai scritto niente...", Toast.LENGTH_SHORT).show();
        }
        else{
            edita.setEnabled(false);
            for(String temp : domanda.getRisposteGiuste()){
                if(edita.getText().toString().equals(temp)){
                    Toast.makeText(this, "Risposta esatta!", Toast.LENGTH_SHORT).show();
                    edita.setTextColor(getResources().getColor(android.R.color.holo_green_dark));
                    punteggio = domanda.getLivello();
                    puntiRimanenti -= punteggio;
                    v.setClickable(false);
                    dont_know = (Button)findViewById(R.id.dont_know);
                    dont_know.setClickable(false);
                    Button but = (Button)findViewById(R.id.next_question);
                    btnMicrophone.setClickable(false);
                    if(ultimaDomanda) but.setText(getText(R.string.last_question));
                    but.setVisibility(View.VISIBLE);
                    if(puntiRimanenti>=-2 && puntiRimanenti<=0){
                        but.setText(getText(R.string.last_question));
                    }
                    return;
                }
            }
            //la risposta editata non è quella giusta
            Toast.makeText(this, "Risposta sbagliata! \nLa risposta era: "+domanda.getRisposteGiuste().get(0), Toast.LENGTH_SHORT).show();
            edita.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            punteggio = 0;
            v.setClickable(false);
            dont_know = (Button)findViewById(R.id.dont_know);
            dont_know.setClickable(false);
            Button but = (Button)findViewById(R.id.next_question);
            btnMicrophone.setClickable(false);
            if(ultimaDomanda) but.setText(getText(R.string.last_question));
            but.setVisibility(View.VISIBLE);
        }
    }

    public void onClickDontKnow(View v){
        punteggio = 0;
        confirm = (Button)findViewById(R.id.confirm);
        confirm.setClickable(false);
        EditText edita = (EditText)findViewById(R.id.your_response);
        edita.setText(domanda.getRisposteGiuste().get(0));

        edita.setEnabled(false);
        Toast.makeText(this, "La risposta era: "+domanda.getRisposteGiuste().get(0), Toast.LENGTH_SHORT).show();
        edita.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
        Button but = (Button)findViewById(R.id.next_question);
        if(ultimaDomanda) but.setText(getText(R.string.last_question));
        but.setVisibility(View.VISIBLE);
        v.setClickable(false);
        btnMicrophone.setClickable(false);
    }

    //Si considera finita una domanda (a cui si è data quindi una risposta: giusta o sbagliata)
    // quando si clicca su "Prossima" (o "Fine"); non quando si clicca su "Conferma"
    public void onClickNextQuestion(View v){
        /*
                If you don't want to return data:
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
         */
        Intent returnIntent = new Intent();
        if(secondiRimanenti==-2){
            returnIntent.putExtra("Secondi",secondiRimanenti);
            returnIntent.putExtra("result", punteggio);
        }
        else {
            returnIntent.putExtra("result", punteggio);
            if (ultimaDomanda) returnIntent.putExtra("Ultima", true);
            if (secondiRimanenti != -1) {
                recuperoSecondiRimanenti();
                returnIntent.putExtra("Secondi", secondiRimanenti);
            }
        }
        if(puntiRimanenti!=-3){
            returnIntent.putExtra("Punti",puntiRimanenti);
        }
        returnIntent.putExtra("quanteDomande",quanteDomande);
        returnIntent.putExtra("quante",quante);
        setResult(Activity.RESULT_OK,returnIntent);
        Log.i(TAG,"Punteggio: "+punteggio);
        Log.i(TAG,"secondiRimanenti: "+secondiRimanenti);
        finish();
    }

    //metodo per gestire il clic da parte dell'utente del back button di android
    @Override
    public void onBackPressed(){
        new AlertDialog.Builder(this)
                .setTitle(R.string.abort)
                .setCancelable(true)
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                    }
                })
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.cancel();
                                Intent returnIntent = new Intent();
                                setResult(Activity.RESULT_CANCELED, returnIntent);
                                finish();
                            }
                        })
                .create()
                .show();
    }

    /**
     * Start speech to text intent. This opens up Google Speech Recognition API dialog box to listen the speech input.
     * */
    private void startSpeechToText() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "Dì la tua risposta...");
        try {
            Log.i(TAG,"startSpeechToText");
            startActivityForResult(intent, SPEECH_RECOGNITION_CODE);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    "Sorry! Speech recognition is not supported in this device.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Callback for speech recognition activity
     * */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case SPEECH_RECOGNITION_CODE: {
                if (resultCode == RESULT_OK && null != data) {
                    if(secondiRimanenti != -1) recuperoSecondiRimanenti();
                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    Log.i(TAG,""+result.toString());
                    EditText edita = (EditText)findViewById(R.id.your_response);
                    if (result.contains(domanda.getRisposteGiuste().get(0))){
                        edita.setText(domanda.getRisposteGiuste().get(0));
                    }
                    else edita.setText(result.get(0));
                }
                break;
            }

        }
    }



}
